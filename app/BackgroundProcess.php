<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackgroundProcess extends Model
{
    protected $table='S_BACKGROUND_PROCESS';
    protected $trim=true;

}
