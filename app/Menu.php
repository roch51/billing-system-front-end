<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table='BR_MENUS';
    public $sequence = 'BR_MENUS_ID_SEQ';
    public function roleMenu() {
        return $this->hasMany('App\RoleMenu');
    }

}
