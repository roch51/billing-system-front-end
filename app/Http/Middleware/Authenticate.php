<?php

namespace App\Http\Middleware;

use App\Menu;
use Closure;
use Auth;
use App\RoleMenu;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $router = app()->make('router');
        $uri = $router->getCurrentRoute()->uri;
        $path = str_replace('/', '.', $uri);
        $path = explode('.', $path);
//        dd($path) ;
        if (session()->has('user')) {
            $roleMenu=RoleMenu::where('role_id',session('role')->role_id)->get();
//        dd($roleMenu);
            $array=[];
            foreach ($roleMenu as $keyaut => $vauth) {
                $array[] = $vauth->menu_id;
            }
//            dd($array) ;
            $routes =Menu::whereIn('id',$array)->get();
            $arraynotdb = array('home');
            $arrayroutes=[];
            $arraystatus=[];
            foreach ($routes as $keyauthrole => $vauthrole) {
                $arrayroutes[] = $vauthrole->route;
                $arraystatus[] = $vauthrole->status;
            }
           $status[]=Menu::whereIn('id',$arraystatus)->first();

            if (!in_array($path[0],array_merge($arrayroutes,$arraynotdb,$status))) {
                abort(404);
            }
            return $next($request);
        }else {
            return redirect('/');
        }

    }
}
