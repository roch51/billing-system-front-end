<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Validator;
use App\UserRole;

class ManageUserController extends Controller
{
    public function index()
    {
        $users = user::join('BR_USER_ROLE', 'BR_USER_ROLE.USER_ID', '=', 'BR_USERS.ID')
            ->join('BR_ROLES', 'BR_ROLES.ID', '=', 'BR_USER_ROLE.ROLE_ID')
            ->select('BR_USERS.NAME', 'BR_USERS.EMAIL', 'BR_USERS.ID', 'BR_ROLES.NAME as ROLE')
//            ->where('BR_USER_ROLE.ROLE_ID' ,'!=', 1)
            ->get();

        $roles = role::get();

        return view('manageUser.index', compact('users', 'roles'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'role' => 'required|not_in:0',
        ]);
        try {
            $userRole = userRole::where('user_id', $request->user_id)->update(['role_id' => $request->role]);
        }catch (\Exception $e){
            return redirect('users/user');
        }

        return redirect('users/user');

    }
}
