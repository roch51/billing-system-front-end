<?php

namespace App\Http\Controllers;

class AccountController extends Controller
{


    public function index() {
        $title = "Account" ;
        return view("account/index",['title' => $title]) ;
    }

    public function accountReview($accountid = null) {
        if ($accountid != null) {
            $title = "Account Review : ".$accountid ;

            $data['InvoicePayments'] = json_decode(app('App\Http\Controllers\PaymentController')->getInvoicePaymentByAccount($accountid),true);
            $data['PaymentReserves'] = json_decode(app('App\Http\Controllers\PaymentController')->getPaymentReserveByAccount($accountid),true);
            $data['Adjustments'] = json_decode($this->getAdjustment($accountid),true);
            $data['Invoices'] = json_decode(app('App\Http\Controllers\InvoiceController')->findByAccounId($accountid),true);
            $data['AccountDetail'] = json_decode($this->getAccount($accountid),true);

            return view("account/review",['title' => $title,'data'=>$data]) ;
        }
    }


    public function getAccount($id = null){
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERLOCAL')."/api/v2/account/".$id;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function getAdjustment($accountId = null){
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERLOCAL')."/api/v2/adjustment/payment-adjustment/".$accountId;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

}
