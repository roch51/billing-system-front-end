<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Role;
use App\RoleMenu;
use Illuminate\Http\Request;

class ManageRoleRouteController extends Controller
{
    public function index()
    {
        $roles = Role::get();
        $menus = Menu::get();
        return view('manageRole.index', compact('roles', 'menus'));
    }

    public function route($id)
    {

        $rolesMenu = RoleMenu::join('BR_MENUS as a', 'a.id', '=', 'BR_ROLES_MENU.MENU_ID')->where('role_id', $id)
            ->select('BR_ROLES_MENU.ROLE_ID', 'a.Name','BR_ROLES_MENU.ID')->get();
//        dd($rolesMenu);

        return $rolesMenu;
    }

    public function detail(Request $request)
    {
        $role = new RoleMenu;
        $role->role_id = $request->role;
        $role->menu_id = $request->menu;
        $role->save();

        return redirect('route');
    }
    public function addRole(Request $request)
    {
        $role = new Role;
        $role->name = $request->nama;
        $role->save();

        return redirect('route');
    }


    public function delete($id)
    {
        $role=RoleMenu::where('id',$id)->delete();

        return redirect('route');
    }


}
