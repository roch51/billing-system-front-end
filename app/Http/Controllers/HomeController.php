<?php

namespace App\Http\Controllers;

use App\BackgroundProcess;
use Illuminate\Http\Request;
use Mockery\Exception;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        dd('a');
        return view('welcome');
    }

    public function terminate($id)
    {
        try {
//            $pid = trim($id);
//            $process = DB::select('Select * FROM S_BACKGROUND_PROCESS where trim(PID) = :id', ['id' => $id]);
//            dd($process);
//            $process = DB::Select('UPDATE S_BACKGROUND_PROCESS SET STATUS = \'TERMINATE\' where trim(PID) = :id', ['id' => $id]);
            $process= BackgroundProcess::where(DB::raw(trim('pid')),$id)->update(['status'=>'TERMINATE']);

            return redirect('home');
        } catch (Exception $e) {
            return redirect('home');
        }
    }

    public function allPosts(Request $request)
    {

        $columns = array(
            0 => 'pid',
            1 => 'process_name',
            2 => 'process_start',
            3 => 'process_end',
            4 => 'status',
            5 => 'description',
            6 => 'pid',
        );
        $totalData = BackgroundProcess::count();
//        dd($totalData);
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
//        dd($request->all());
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = BackgroundProcess::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = BackgroundProcess::where('pid', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = BackgroundProcess::where('pid', 'LIKE', "%{$search}%")
                ->count();
        }
        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {

                $nestedData['pid'] = trim($post->pid);
                $nestedData['process_name'] = $post->process_name;
                $nestedData['process_start'] = $post->process_start;
                $nestedData['process_end'] = $post->process_end;
                $nestedData['status'] = $post->status;
                $nestedData['description'] = $post->description;
                $nestedData['pid'] = trim($post->pid);

                $data[] = $nestedData;

            }
        }
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }
}
