<?php

/**
 * Application Enginnering
 */

namespace App\Http\Controllers\Authenticate;

use App\UserRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Http\Controllers\Authenticate\cekldapController;
use App\User;
use App\RoleMenu;
use Auth;
use Symfony\Component\HttpFoundation\Session\Session;
//Class needed for login and Logout logic
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    //Trait

    protected function guard()
    {
        return Auth::guard('authenticate');
    }

    public function index()
    {
//        session()->flush();
//        dd(session('role'));
        if (count(session('user')) > 0) {
            return redirect('/home');
        } else {
//            session()->flush();
            return view('authenticate.login');
        }
    }

    public function postlogin(Request $request)
    {
        try {

            $hasher = app()->make('hash');
            $email = $request->email;
            $password = $request->password;

            $tmp = explode('@', $email);

            if (trim($email == "" || trim($password) == "" || count($tmp) == 0)) {

                return redirect('/')->with('success', 'unknown User');
            }

            $vt = addslashes(trim($tmp[1])); //CEK DULU MNC BUKAN
            $vt_user = addslashes(trim($tmp[0]));
            if ($vt == 'mncplaymedia.com') {
                $result = cekldapController::x_get_groups($vt_user, $password);
            } else {
                $result = array('group' => false, 'email' => false, 'username' => false, 'cekldap' => false);
            }

//            $result = array('email' =>'ccr_admin_user@mncplaymedia.com', 'cekldap' => true);
            $cekuser = User::where('email', $result['email'])->first();
//            dd($cekuser);
            if ($result['cekldap'] == true) {                                       // LEVEL 1  LDAP
                if (count($cekuser) > 0) {
                    $role = UserRole::where('user_id', $cekuser->id)->first();
//                                dd($role);
                    $menu = RoleMenu::where('BR_Roles_Menu.role_id', $role->role_id)
                        ->where('br_menus.status','0')
                        ->join('BR_Menus', 'BR_Menus.id', '=', 'BR_Roles_Menu.menu_id')
                        ->get();
                    $submenu = RoleMenu::where('BR_Roles_Menu.role_id', $role->role_id)
                        ->where('br_menus.status','!=',0)
                        ->join('BR_Menus', 'BR_Menus.id', '=', 'BR_Roles_Menu.menu_id')
                        ->get();
//                    dd($menu);
                    $request->session()->put('user', $cekuser);
                    $request->session()->put('role', $role);
                    $request->session()->put('menu', $menu);
                    $request->session()->put('submenu', $submenu);
                    return redirect('/home');
                } else { //REGISTER OTOMATIS
//               ADD USER

                    DB::beginTransaction();

                    $a = new User;
                    $a->name = $result['username'];
                    $a->email = $result['email'];
                    $a->PASSWORD = bcrypt('ldap');
                    $a->save();
                    $iduser = $a->id;
                    $b = new UserRole;
                    $b->user_id = $iduser;
                    $b->role_id = 2;
                    $b->save();

                    DB::commit();
                    $cekuser = User::where('id', $iduser)->first();
                    $role = UserRole::where('user_id', $cekuser->id)->first();
                    $menu = RoleMenu::where('BR_Roles_Menu.role_id', $role->role_id)
                        ->where('br_menus.status','0')
                        ->join('BR_Menus', 'BR_Menus.id', '=', 'BR_Roles_Menu.menu_id')
                        ->get();
                    $submenu = RoleMenu::where('BR_Roles_Menu.role_id', $role->role_id)
                        ->where('br_menus.status','!=',0)
                        ->join('BR_Menus', 'BR_Menus.id', '=', 'BR_Roles_Menu.menu_id')
                        ->get();
                    $request->session()->put('user', $cekuser);
                    $request->session()->put('role', $role);
                    $request->session()->put('menu', $menu);
                    $request->session()->put('submenu', $submenu);
//                    dd(session('user'));
                    return redirect('/');
                }
            } else { // $result['cekldap'] ==  false


                session()->put('respon_msg', ['msg' => 'email and password incorrect', 'code' => 'warning']);
                $s = session('respon_msg');
                // dd($s['msg']);
                DB::rollback();
                return redirect('/');
                // ->with('success', 'email and  password is incorrect')

            }
        } catch (Exception $ex) {

            session()->put('respon_msg', ['msg' => 'email and  password is incorrect', 'code' => 'warning']);
            session()->flush();
            DB::rollback();
            return redirect('/')->with('success', 'email and  password is incorrect');
        }
    }

    public function logout()
    {
        Auth::logout();
        \Session::flush();
        return redirect('/');
    }


}
