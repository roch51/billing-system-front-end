<?php
/**
 * Application Enginnering
 */

namespace App\Http\Controllers\Authenticate;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class cekldapController extends Controller
{

    public static function x_get_groups($vt_user, $password)
    {

        try {
            $ldap_host = "mncplaymedia.com";
            $host = "10.9.35.26";
            $ldap_dn = "DC=mncplaymedia,DC=com";
            $query_user = $vt_user . "@" . $ldap_host;
            $ldap = ldap_connect($host) or die("Tidak dapat terkoneksi ke LDAP, silahkan hubungi team IT");
            ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
            ldap_set_option($ldap, LDAP_OPT_TIMELIMIT, 1);
            ldap_set_option($ldap, LDAP_OPT_NETWORK_TIMEOUT, 1);

            if (@ldap_bind($ldap, $query_user, $password) == false) {
                return array('group' => false, 'email' => false, 'username' => false, 'cekldap' => false);
            }


            $results = ldap_search($ldap, $ldap_dn, "(samaccountname={$vt_user})", array("memberof"));
            $entries = ldap_get_entries($ldap, $results);

            if ($entries['count'] < 1) {

                return array('group' => false, 'email' => false, 'username' => false, 'cekldap' => false);
            }

            return array('email' => $vt_user . '@mncplaymedia.com', 'username' => $vt_user, 'cekldap' => true);
            @ldap_close($ldap);
        } catch (Exception $ex) {
            echo 'ERROR LDAP';
        }
    }

}
