<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AccountBalanceController extends Controller
{
    public function index() {

        $title = "Account Balance" ;
        return view("accountbalance/index",['title' => $title]) ;
    }

    public function detail($accountId = null) {
        $title = "Detail Account Balance " ;
        $data['AccountDetail'] = json_decode($this->apiDetailAccount($accountId)) ;
        $data['AccountBalance'] = json_decode($this->accountBalanceDetail($accountId)) ;
//        dd($data);
        return view("accountbalance/detail",['title' => $title,'data'=>$data]) ;
    }


    public function getData($page = null, $size = null){
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERDEV')."/api/v2/accountbalance?page=".$page."&size=".$size ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        return ($result) ;
    }
    public function history($id = null,$page = null, $size = null){
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERDEV')."/api/v2/accountbalance/history/".$id."/?page=".$page."&size=".$size ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
//        dd($result);
        return ($result) ;
    }

    public function getAccountBalance($accountId = null){
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERDEV')."/api/v2/accountbalance/".$accountId ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function apiDetailAccount($accountId = null){
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERDEV')."/api/v2/billingresource/account/detail/".$accountId;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }
    public function accountBalanceDetail($accountId = null){
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERDEV')."/api/v2/accountbalance/detail/".$accountId;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function getAllData(Request $request){
        ini_set('max_execution_time', 0);
        $params = json_encode($request->all(),JSON_PRETTY_PRINT) ;
        $server = getenv('SERVERDEV')."/api/v2/accountbalance/all" ;
        $ch = curl_init($server);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($params))
        );
        $result = curl_exec($ch);
        return ($result) ;
    }
}
