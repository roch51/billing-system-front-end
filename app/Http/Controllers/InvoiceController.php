<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function index()
    {
        $title = "Invoice";
        $mbillingCycles = json_decode($this->optBillingCycle());
        return view("invoice/index", ['title' => $title, 'mbillingCycles' => $mbillingCycles]);
    }

    public function summary($generateNo)
    {
        $title = "Invoice Summary";
        return view("invoice/invoice-summary", ['title' => $title, 'generateNo' => $generateNo]);
    }

    public function detailInvoice($id = null) {
        $title = "Detail Invoice";
        $data = json_decode($this->apiDetailInvoice($id),true) ;
        return view("invoice/invoice-detail", ['title' => $title, 'data' => $data]);
    }

    public function pdfInvoice($id = null) {
        $title = "PDF Invoice";
        $data = json_decode($this->apiDetailInvoice($id),true) ;
        return view("invoice/invoice-pdf", ['title' => $title, 'data' => $data]);
    }


    public function loadGenerate(Request $request)
    {
        ini_set('max_execution_time', 0);
        $params = json_encode($request->all(),JSON_PRETTY_PRINT) ;
        $server = getenv('SERVERDEV')."/api/v2/billingresource/invoice" ;

        $ch = curl_init($server);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($params))
        );
        $result = curl_exec($ch);
        return ($result) ;
    }

    public function apiSumaryInvoice($generateNo = null, Request $request)
    {
        ini_set('max_execution_time', 0);
        $params = json_encode($request->all(),JSON_PRETTY_PRINT) ;
        $server = getenv('SERVERDEV')."/api/v2/billingresource/invoice/".$generateNo ;
        $ch = curl_init($server);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($params))
        );
        $result = curl_exec($ch);
        return ($result) ;
    }


    public function loadInvoice($page, $size)
    {
        $server = getenv('SERVERDEV') . "/api/v2/invoice?page=" . $page . "&size=" . $size . "&sort=generateNo";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }

    public function generateInvoice($generateNo = null)
    {
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERDEV') . "/api/v2/billingresource/invoice/" . $generateNo;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }


    public function apiDetailInvoice($summaryNo = null)
    {
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERDEV') . "/api/v2/billingresource/invoice/detail/" . $summaryNo;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }

    public function apiDetailAccount($accountId = null)
    {
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERDEV') . "/api/v2/account/" . $accountId;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }

    public function apiAccountItem($generateAccountId = null)
    {
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERDEV') . "/api/v2/invoice/detail-item/" . $generateAccountId;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }

    public function apiInvoicePayment($accountId = null)
    {
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERDEV') . "/api/v2/account/" . $accountId;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }

    public function findByIdAndPeriod($accountId = null, $period = null)
    {
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERDEV') . "/api/v2/invoice/account/" . $accountId . "/period/" . $period;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }

    public function optBillingCycle()
    {
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERDEV') . "/api/v2/billingcycle/list";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }

    public function findByAccounId($accountId = null)
    {
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERDEV') . "/api/v2/invoice/account/" . $accountId;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }

    public function loadInvoiceSummary($generateNo = null)
    {
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERDEV') . "/api/v2/billingresource/invoice/summary/" . $generateNo ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }

    public function generatePdfInvoice($invoiceSummaryId = null)
    {
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERDEV') . "/api/v2/billingresource/invoice/single-invoice/" . $invoiceSummaryId ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }

    public function generatePdfInvoices($generateNo = null)
    {
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERDEV') . "/api/v2/billingresource/invoice/jasper/" . $generateNo ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }

    public function generatePdfCount($generateNo = null) {
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERDEV') . "/api/v2/billingresource/invoice/pdfcount/" . $generateNo ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }

    public function generatePdfAllSelect($generateNo = null) {
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERDEV') . "/api/v2/billingresource/invoice/summary/" . $generateNo ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }
}
