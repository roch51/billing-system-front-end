<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;

class ManageMenuController extends Controller
{
    public function index()
    {
        $menus = Menu::where('status', 0)->get();
        return view('manageMenu.index', compact('menus', 'childMenus'));
    }

    public function child($id)
    {
        $Menus = Menu::where('status', '!=', 0)->where('status', $id)->get();
        $childMenus = array($Menus);
        return $childMenus;
    }

    public function delete($id)
    {
//        dd($id);
        $Menus = Menu::where('id', $id)->delete();
        return redirect('menu');
    }

    public function addChild(Request $request)
    {
//        dd($request->all());
        $menus = new Menu;
        $menus->name = $request->nama;
        $menus->route = $request->route;
        $menus->status = $request->parrent;
        $menus->save();
        return redirect('menu');
    }

    public function addParrent(Request $request)
    {

        $menus = new Menu;
        $menus->name = $request->nama;
        $menus->route = $request->route;
        $menus->status = 0;
        $menus->save();
        return redirect('menu');
    }
}
