<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class PaymentController extends Controller
{
    public function bss1Reserve() {
        $title = "Bss1 Payment Reserve Sync" ;
        return view("payment/bss1-payment-reserve",['title' => $title]) ;
    }

    public function bss1Invoice() {
        $title = "Bss1 Invoice Payment Sync" ;
        return view("payment/bss1-invoice-payment",['title' => $title]) ;
    }

    public function syncDataFirstbill($from = null, $to = null) {
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERLOCAL').'/api/v2/payment/payment-reserve/batch/'.$from."/".$to ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function syncDataPayment($from = null, $to = null) {

        ini_set('max_execution_time', 0);
        $server = getenv('SERVERLOCAL').'/api/v2/payment/invoice-payment/batch/'.$from."/".$to ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function getPrevPayment($from = null, $to=null){
        ini_set('max_execution_time', 0);
        $server = "http://bscheck.mncplaymedia.com:8080/checking/api/v1/payment/invoice-payment/".$from."/".$to;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function getPrevFirstbill($from = null, $to=null){
        ini_set('max_execution_time', 0);
        $server = "http://bscheck.mncplaymedia.com:8080/checking/api/v1/payment/payment-reserve/".$from."/".$to;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function syncDataPaymentPost($from = null, $to = null) {

        $data = $this->getPrevPayment($from,$to) ;

        $ch = curl_init(getenv('SERVERLOCAL').'/api/v2/payment/invoice-payment/batch/');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        $result = curl_exec($ch);
        return $result ;
    }

    public function syncDataFirstbillPost($from = null, $to = null) {

        $data = $this->getPrevFirstbill($from,$to) ;

        $ch = curl_init(getenv('SERVERLOCAL').'/api/v2/payment/payment-reserve/batch/');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        $result = curl_exec($ch);
        return $result ;
    }

    public function getInvoicePaymentByAccount($accountId = null){
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERLOCAL')."/api/v2/payment/invoice-payment/".$accountId;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function getPaymentReserveByAccount($accountId = null){
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERLOCAL')."/api/v2/payment/payment-reserve/".$accountId;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }
}
