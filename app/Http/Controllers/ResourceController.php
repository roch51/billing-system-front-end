<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResourceController extends Controller
{
    public function index()
    {
        $title = "Resource";
        return view("resource/index", ['title' => $title]);
    }

    public function detail($id)
    {
        $title = "Resource";
        $data = json_decode($this->detailAccount($id), true);
        return view("resource/detail", ['title' => $title,'data'=>$data]);
    }

    public function restAccounts(Request $request)
    {
        ini_set('max_execution_time', 0);
        $params = json_encode($request->all(),JSON_PRETTY_PRINT) ;
        $server = getenv('SERVERDEV')."/api/v2/billingresource/account/all" ;

        $ch = curl_init($server);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($params))
        );
        $result = curl_exec($ch);
        return ($result) ;
    }

    public function findAccount($id)
    {
        $server = getenv('SERVERDEV') . "/api/v2/billingresource/order/findaccount/" . $id;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    public function listDiscount($id)
    {
        $server = getenv('SERVERDEV') . "/api/v2/billingresource/discount/list/" . $id;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    public function detailAccount($id)
    {
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERDEV')."/api/v2/billingresource/account/detail/".$id;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }
}
