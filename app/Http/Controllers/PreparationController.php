<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller ;
use Excel;

class PreparationController extends Controller
{
    public function index() {
        $title = "Data Preparation ";
        $mbillingCycles = json_decode($this->optBillingCycle()) ;

        return view("preparation/index", ['title' => $title,'mbillingCycles' => $mbillingCycles]) ;
    }

    public function listGenerated() {
        $title = "Data After Preparation ";

        return view("preparation/list-generated", ['title' => $title]) ;
    }

    public function listAccount($generateNo = null,$status = null) {
        $title = "Data After Preparation - Account";
        $disabled = "" ;
        if ($status == "INVOICED") {
            $disabled = "disabled" ;
        }
        return view("preparation/list-account", ['title' => $title, 'generateNo'=>$generateNo,'status'=>$disabled]) ;
    }

    public function detailItems($generateAccountId = null) {
        if ($generateAccountId != null) {
            $title = "Data After Preparation - Account";
            $data["items"] = json_decode($this->generatedAccountItems($generateAccountId),true) ;

            return view("preparation/list-items", ['title' => $title, 'data'=>$data]) ;
        }
    }

    public function firstBill() {
        $title = "Data Preparation First Bill";

        return view("preparation/firstbill-index", ['title' => $title]) ;
    }

    public function createTemp($accountId = null, $period = null){

        $data["accountId"] = $accountId ;
        $data["period"] = $period ;

        $sendData = json_encode($data) ;

        $ch = curl_init(getenv('SERVERLOCAL').'/api/v2/first-bill/create-tmp');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $sendData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($sendData))
        );
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        return response($result, $httpcode);
    }



    public function generatedData($periode, $bc) {
        $check = $this->checkGenerate($periode,$bc);
        $status = ["status"=>""] ;
        if ($check) {
            $status = $this->generateData($periode,$bc) ;
        }else {
            $status = json_encode(["status"=>"already generated !!"]);
        }
        return ($status) ;
    }

    public function process($periode, $bc) {
        $title = "Data Preparation Wizard" ;

        return view("preparation/process", ['title' => $title,'periode'=>$periode,'bc'=>$bc]) ;
    }

    public function generateAccount() {
        $title = "Data Preparation Per Account" ;

        return view("preparation/preparation-account",['title' => $title]) ;
    }

    public function syncDataFirstbill($from = null, $to = null) {

        $data = $this->getPrevFirstbill($from,$to) ;

        $ch = curl_init(getenv('SERVERLOCAL').'/api/v2/payment/payment-reserve/batch');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        $result = curl_exec($ch);
        return $result ;
    }

    public function syncDataPayment($from = null, $to = null) {

        $data = $this->getPrevPayment($from,$to) ;

        $ch = curl_init(getenv('SERVERLOCAL').'/api/v2/payment/invoice-payment/batch');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        $result = curl_exec($ch);
        return $result ;
    }

    public function restData($page = null, $size = null){
        $server = getenv('SERVERLOCAL')."/api/v2/billing?page=".$page."&size=".$size."&sort=generateNo" ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function checkGenerate($periode = null, $bc = null){
        $server = getenv('SERVERLOCAL')."/api/v2/billing/check-generated/".$periode."/".$bc ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result ;
    }

    public function generateData($periode = null, $bc = null){
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERLOCAL')."/api/v2/billing/generate/".$periode."/".$bc ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function getPrevPayment($from = null, $to=null){
        ini_set('max_execution_time', 0);
        $server = "http://bscheck.mncplaymedia.com:8080/checking/api/v1/payment/invoice-payment/".$from."/".$to;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function getPrevFirstbill($from = null, $to=null){
        ini_set('max_execution_time', 0);
        $server = "http://bscheck.mncplaymedia.com:8080/checking/api/v1/payment/payment-reserve/".$from."/".$to;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function getAllAccount(){
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERLOCAL')."/api/v2/account";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function getAccountByName($fullname = null, $page = null, $size = null){

        ini_set('max_execution_time', 0);
        $server = getenv('SERVERLOCAL')."/api/v2/account/find/".$fullname."?page=".$page."&size=".$size."&sort=fullname" ;
        $server = str_replace(" ", '%20', $server);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function generatedPerAccount($periode = null , $accountId = null) {
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERLOCAL')."/api/v2/billing/generate-account/".$periode."/".$accountId ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function optBillingCycle() {
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERLOCAL')."/api/v2/billingcycle/list";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }
    public function preparationBatch() {
        $title = "Data Preparation Batch";
        return view("preparation/preparation-batch", ['title' => $title]) ;
    }
    public function excelImport(Request $request) {
        if ($request->file('excel')) {
            $path = $request->file('excel')->path() ;
            $period = $request->input('periode') ;
            $results = Excel::load($path, function($reader){})->get();
            $items = [] ;
            foreach ($results->toArray() as $result) {
                $result['period'] = $period ;
                $result['status'] = json_decode($this->checkInvoice($result['period'],$result['accountid']))->status ;
                array_push($items,$result) ;
            }
            $datas = [] ;
            foreach ($items as $item) {
                if ($item['status'] == 'true') {
                    unset($item['status']);
                    array_push($datas,$item) ;
                }
            }

            $data = json_encode($datas) ;

            $ch = curl_init(getenv('SERVERLOCAL').'/api/v2/billing/generate/batch');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
            );
            $result = curl_exec($ch);
            $title = "Preparation Batch" ;
            return view("preparation/batch-finish", ['title' => $title,'result' => json_decode($result),'items'=>$items]) ;
        }
    }

    public function checkInvoice($period,$accountId) {
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERLOCAL')."/api/v2/invoice/check-invoice/".$period."/".$accountId;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function listGeneratedData($period = null ,$page = null, $size = null){
        $server = getenv('SERVERLOCAL')."/api/v2/billing/after-generated/".$period."?page=".$page."&size=".$size."&sort=generateNo" ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function listGeneratedAccount($generatedNo = null ,$page = null, $size = null){
        $server = getenv('SERVERLOCAL')."/api/v2/billing/generate-account/".$generatedNo."?page=".$page."&size=".$size ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function changeStatus(Request $request){
        $data['id'] = $request['id'];
        $data['documentStatus'] =  $request['statusDocumentId'] ;

        $server = getenv('SERVERLOCAL').'/api/v2/billing/generate-account/'.$data['id']."/status/".$data['documentStatus'];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function generatedAccountItems($generateAccountId = null) {
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERLOCAL')."/api/v2/billing/generate-account-item/".$generateAccountId ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function searchGenerateAccount($generateAccountId = null,$accountId = null) {
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERLOCAL')."/api/v2/billing/generate-account/".$generateAccountId."/account/".$accountId ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function tmpList(){
        $server = getenv('SERVERLOCAL')."/api/v2/first-bill/tmplist/";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function generateFirstbill($periode = null) {
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERLOCAL')."/api/v2/first-bill/generate/".$periode;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function deleteTemp() {
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERLOCAL')."/api/v2/first-bill/delete-tmp";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        return response($result, $httpCode);
    }

    public function deleteTempAccount($accountId = null) {
        ini_set('max_execution_time', 0);

        $server = getenv('SERVERLOCAL')."/api/v2/first-bill/delete-tmp/".$accountId;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        return response($result, $httpCode);
    }
}
