<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mockery\Exception;

class OrderController extends Controller
{
    public function indexPayment()
    {
        $title = "Payment";
        $mbillingCycles = json_decode($this->optBillingCycle());
        $mchannel = json_decode($this->optChannel());
        $mchannelInfo = json_decode($this->optChannelInfo());

        return view("order.indexPayment", ['title' => $title, 'mbillingCycles' => $mbillingCycles,'channel'=>$mchannel,'channelInfo'=>$mchannelInfo]);
    }

    public function indexReversal()
    {
        $title = "Reversal";
        $mbillingCycles = json_decode($this->optBillingCycle());

        return view("order.indexReversal", ['title' => $title]);
    }

    public function indexAdjustment()
    {
        $title = "Adjustment";
        $mbillingCycles = json_decode($this->optBillingCycle());

        return view("order.indexAdjustment", ['title' => $title]);
    }

    public function postPayment(Request $request)
    {
//        dd($request->all());
        $data = json_encode($request->data);
        try {
            $ch = curl_init(getenv('SERVERDEV') . '/api/v2/billingresource/payment/single-payment');

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
            );
            $result = curl_exec($ch);
            $request->session()->flash('success', 'Payment successfully added!');
            return redirect('payment');
        } catch (Exception $e) {
            $request->session()->flash('warning', 'Payment failed!');
            return redirect('payment');
        }

    }
    public function postAdjustment(Request $request)
    {
        $data = json_encode($request->data);
//        dd($data);
        try {
            ini_set('max_execution_time', 0);

            $ch = curl_init(getenv('SERVERDEV') . '/api/v2/billingresource/adjustment/single-adjustment');
//            dd($ch);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
            );
            $result = curl_exec($ch);
            $request->session()->flash('success', 'Adjustment successfully added!');
            return redirect('adjustment');
        } catch (Exception $e) {
            $request->session()->flash('warning', 'Adjustment  failed!');
            return redirect('adjustment');
        }

    }
    public function postReversal(Request $request)
    {
        $acc_id=$request->data['account_id'];
        $refid=$request->data['reference_id'];
        try {
            $ch = curl_init(getenv('SERVERDEV') . '/api/v2/billingresource/payment/reversal/'.$acc_id.'/'.$refid);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json'
            ));
            $result = curl_exec($ch);
            $request->session()->flash('success', 'Reversal successfully added!');
            return redirect('reversal');
        } catch (Exception $e) {
            $request->session()->flash('success', 'Reversal  failed!');
            return redirect('reversal');
        }

    }

    public function optBillingCycle()
    {
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERDEV') . "/api/v2/billingcycle/list";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }

    public function optChannel()
    {
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERDEV') . "/api/v2/channel";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }
    public function getChannel($id)
    {
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERDEV') . "/api/v2/channel/".$id;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }
    public function optChannelInfo()
    {
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERDEV') . "/api/v2/channel-info";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result);
    }
}
