<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;

class GenerateInvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('authenticate');
    }

    public function index(){
        $title = "Generate Invoice";
        return view("generate/index", ['title' => $title]) ;
    }

    public function runbc(){
        $title = "Generate Invoice - Billing Cycle";
        $mbillingCycles = json_decode($this->optBillingCycle()) ;
        return view("generate/runbc", ['title' => $title,'mbillingCycles' => $mbillingCycles]) ;
    }

    public function firstbill(){
        $title = "Generate Invoice - First Bill";

        return view("generate/firstbill", ['title' => $title]) ;
    }

    public function batchSave(Request $request){
        if ($request->file('excel')) {
            $path = $request->file('excel')->path() ;
            $period = $request->input('periode') ;
            $results = Excel::load($path, function($reader){})->get();
            $data = json_encode($results->toArray()) ;


            $ch = curl_init(getenv('SERVERDEV').'/api/v2/billingresource/invoice/batch/'.$period);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
            );
            $result = curl_exec($ch);


            $title = "Batch Finish" ;
            return view("generate/batch-finish", ['title' => $title,'result' => json_decode($result)]) ;
        }
    }

    public function batch(){
        $title = "Generate Invoice - Batch";
        return view("generate/batch", ['title' => $title]) ;
    }

    public function optBillingCycle() {
        ini_set('max_execution_time', 0);
        $server = getenv('SERVERDEV')."/api/v2/billingcycle/list";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $server);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return ($result) ;
    }

    public function executeRunbill($period = null , $bc = null) {
        if ($period != null && $bc != null) {
            ini_set('max_execution_time', 0);
            $server = getenv('SERVERDEV')."/api/v2/billingresource/invoice/runbill/".$period."/".$bc;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $server);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($curl);
            curl_close($curl);

            return ($result) ;
        }
    }

    public function executeFirstbill($accountId = null) {
        if ($accountId != null) {
            ini_set('max_execution_time', 0);
            $server = getenv('SERVERDEV')."/api/v2/billingresource/invoice/firstbill/".$accountId;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $server);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($curl);
            curl_close($curl);

            return ($result) ;
        }
    }
}
