<?php
/**
 * Created by PhpStorm.
 * User: Ikhsan Nugraha M
 * Date: 03/11/2017
 * Time: 18:57
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function __construct()
    {

        $this->middleware('authenticate');

    }

    public function getIndex()
    {

        return view('manageUser.index');
    }



}





