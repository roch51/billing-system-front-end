<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table='BR_ROLES';
    public $sequence = 'BR_ROLE_ID_SEQ';

    public function userRole() {
        return $this->hasMany('App\UserRole');
    }

    public function roleMenu() {
        return $this->hasMany('App\RoleMenu');
    }

}

