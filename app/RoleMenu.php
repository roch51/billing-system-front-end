<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
    protected $table='BR_ROLES_MENU';
    public $sequence = 'BR_ROLES_MENU_ID_SEQ';
    public function menu() {
        return $this->belongsTo('App\Menu');
    }

    public function role() {
        return $this->belongsTo('App\Role');
    }
}
