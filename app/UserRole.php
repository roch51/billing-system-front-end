<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table='BR_USER_ROLE';
    public $sequence = 'BR_USER_ROLE_ID_SEQ';


    public function user() {
        return $this->belongsTo('App\User');
    }
    public function role() {
        return $this->belongsTo('App\Role');
    }



}
