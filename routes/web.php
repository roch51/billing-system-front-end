<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('/', 'Authenticate\LoginController');
Route::post('/postlogin', 'Authenticate\LoginController@postlogin');
Route::get('/postlogout', 'Authenticate\LoginController@logout');

Route::group(['middleware' => 'authenticate'], function () {
    Route::get('/bssinvoice', 'BsInvoiceController@lists');
    Route::get('/home', 'HomeController@index');
    Route::post('/home/allposts', 'HomeController@allPosts');
    Route::get('/home/terminate/{id}', 'HomeController@terminate');
    //Billing Invoice Routing
    Route::get('/invoice', 'InvoiceController@index');
    Route::post('/invoice/loadgenerate', 'InvoiceController@loadGenerate');
    Route::get('/invoice/summary/{generateNo}', 'InvoiceController@summary');
    Route::post('/invoice/summary/rest/{generateNo}', 'InvoiceController@apiSumaryInvoice');
    Route::get('/invoice/summary/detail/{summaryNo}', 'InvoiceController@detailInvoice');
    Route::get('/invoice/summary/pdf/{summaryNo}', 'InvoiceController@pdfInvoice');

    Route::get('/invoice/pdf/all/{id}', 'InvoiceController@generatePdfInvoices');
    Route::get('/invoice/pdf/single/{id}', 'InvoiceController@generatePdfInvoice');
    Route::get('/invoice/pdf/count/{id}', 'InvoiceController@generatePdfCount');
    Route::get('/invoice/pdf/select/{id}', 'InvoiceController@generatePdfAllSelect');

    //Billing Payment Routing
    Route::get('/payment/bss1-reserve', 'PaymentController@bss1Reserve');
    Route::get('/payment/bss1-invoice', 'PaymentController@bss1Invoice');
    Route::get('/payment/invoicepayment/{from}/{to}', 'PaymentController@syncDataPayment');
    Route::get('/payment/firstbill/{from}/{to}', 'PaymentController@syncDataFirstbill');

    //Billing Scheduler Routing
    Route::get('/scheduler', 'SchedulerController@index');

    //Account Balance Routing
    Route::get('/account-balance', 'AccountBalanceController@index');
    Route::get('/account-balance/detail/{accountId}', 'AccountBalanceController@detail');
    Route::get('/account-balance/rest/{page}/{size}', 'AccountBalanceController@getData');
    Route::get('/account-balance/history/{id}/{page}/{size}', 'AccountBalanceController@history');
    Route::post('/account-balance/rest/all', 'AccountBalanceController@getAllData');

    //Account
    Route::get('/account/{id}', 'AccountController@getAccount');
    Route::get('/account', 'AccountController@index');
    Route::get('/account/review/{accountid}', 'AccountController@accountReview');

    //users
    Route::get('/users', 'UserController@getIndex');
    Route::get('/user/create', 'UserController@create');
    Route::post('/user/created/', 'UserController@createPost');
    //Role
    Route::get('/role/create', 'UserController@create');
    Route::post('/role/created/', 'UserController@createPost');

    //generate
    Route::get('/generate', 'GenerateInvoiceController@Index');
    Route::get('/generate/runbc', 'GenerateInvoiceController@runbc');
    Route::get('/generate/runbc/exec/{period}/{bc}', 'GenerateInvoiceController@executeRunbill');
    Route::get('/generate/firstbill', 'GenerateInvoiceController@firstbill');
    Route::get('/generate/firstbill/exec/{accountId}', 'GenerateInvoiceController@executeFirstbill');
    Route::get('/generate/batch', 'GenerateInvoiceController@batch');
    Route::post('/generate/batch/save', 'GenerateInvoiceController@batchSave');

    //users
    Route::get('/user', 'ManageUserController@Index');
    Route::post('/user/update', 'ManageUserController@update');

    //route
    Route::get('/route', 'ManageRoleRouteController@Index');
    Route::post('/route/add', 'ManageRoleRouteController@detail');
    Route::post('/route/role/add', 'ManageRoleRouteController@addRole');
    Route::get('/route/role/{id}', 'ManageRoleRouteController@route');
    Route::get('/route/role/delete/{id}', 'ManageRoleRouteController@delete');


    //menu
    Route::get('/menu', 'ManageMenuController@Index');
    Route::get('/menu/child/{id}', 'ManageMenuController@child');
    Route::post('/menu/child/add', 'ManageMenuController@addChild');
    Route::post('/menu/parrent/add', 'ManageMenuController@addParrent');
    Route::get('/menu/child/delete/{id}', 'ManageMenuController@delete');

    //br_resource
    Route::get('/resource', 'ResourceController@Index');
    Route::get('/resource/detail/{id}', 'ResourceController@detail');
    Route::get('/resource/findaccount/{id}', 'ResourceController@findAccount');
    Route::get('/resource/listdiscount/{id}', 'ResourceController@listDiscount');
    Route::get('/resource/detailaccount/{id}', 'ResourceController@detailAccount');
    Route::post('/resource/accounts/rest', 'ResourceController@restAccounts');

    //order
    Route::get('/payment', 'OrderController@IndexPayment');
    Route::get('/payment/channel/{id}', 'OrderController@getChannel');
    Route::get('/payment/sub-channel/{id}', 'OrderController@getChannel');
    Route::post('/payment', 'OrderController@PostPayment');

    Route::get('/reversal', 'OrderController@indexReversal');
    Route::post('/reversal', 'OrderController@postReversal');

    Route::get('/adjustment', 'OrderController@indexAdjustment');
    Route::post('/adjustment', 'OrderController@postAdjustment');




});