<?php
/**
 * Created by PhpStorm.
 * User: Arifin
 * Date: 27/06/2018
 * Time: 19.54
 */


$env = $app->detectEnvironment(function(){
    $envPath = trim(__DIR__.'/../.env') ;
    $setEnv = trim(file_get_contents($envPath)) ;
    if (file_exists($envPath)) {
        $putenv = putenv("APP_ENV=$setEnv");
        if (getenv('APP_ENV') && file_exists(__DIR__.'/../.'.getenv('APP_ENV').'.env')){
            $dotEnv = new \Dotenv\Dotenv(__DIR__.'/../','.' .getenv('APP_ENV').'.env');
            if (file_exists('.env')) {
               $dotEnv->load() ;
            }
        }
    }
}) ;