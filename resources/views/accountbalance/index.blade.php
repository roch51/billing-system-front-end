@extends('layouts.master')

@section('content')
    <div class="card">
        <div class="card-header no-bg b-a-0">
            Data Account Balance
        </div>
        <div class="card-block">
            <div class="table-responsive">
                <table id="table-data" class="table table-bordered table-striped m-b-0" style="width:100%">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Account Id</th>
                        <th>Amount</th>
                        <th>Update Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function(){

           var table = $("#table-data").DataTable({
                // dom: "Bfrtip",
                // responsive: true,
                // select: true,
                // filter: true,
                // paging: true,
                // start : 0,
                // pageLength: 20,
                serverSide: true,
                processing: true,
                // stateSave : true,
                // deferRender : true,
                // lengthMenu :[10, 20, 50, 100],
                order: [[1, "desc"]],
                buttons: [],
                ajax: {
                    url: "<?php echo url('/')?>/account-balance/rest/all",
                    // dataSrc: '',
                    type : "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // data : function (data) {
                    //     return JSON.stringify(data) ;
                    // }
                },
                columns: [
                    {data: "id", name: "id", sortable: true,searchable:false},
                    {data: "accountId", name: "accountId", sortable: true},
                    {data: "amount", name: "amount", sortable: false},
                    {data: "updatedDate", name: "updatedDate", sortable: false},
                    {data: "accountId", name: "account_id", className: "dt-body-center", render: function(data, type, row, meta) {
                            return '<a href="/account-balance/detail/'+data+'"><button class="btn btn-outline-primary m-r-xs m-b-xs btn-sm">detail</button></a>' ;
                            }, sortable: false, searchable: false}

                ]
            });


        });
    </script>
@endsection