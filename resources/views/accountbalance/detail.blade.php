@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header no-bg b-a-0"></div>
                <div class="card-block">
                    <form id="preparationform" data-toggle="validator" role="form">
                        <div class="form-group">
                            <label for="exampleInputEmail1">
                                Account Id
                            </label>
                            <input class="form-control" id="account-id"
                                   placeholder="Account ID" required="" type="current" value="{{$data["AccountBalance"]!=null?$data["AccountBalance"]->accountId:"-"}}" readonly="true"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">
                                Account Name
                            </label>
                            <input class="form-control" id="account-name"
                                   placeholder="Account Name" required="" type="current" value="{{$data["AccountDetail"]!=null?$data["AccountDetail"]->contact_name:"-"}}" readonly="true"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputAddress">
                                Account Address
                            </label>
                            <input class="form-control" id="account-address"
                                   placeholder="Account Address" required="" type="current" value="{{$data["AccountDetail"]!=null?$data["AccountDetail"]->address:"-"}}" readonly="true"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPhone">
                                Account Phone
                            </label>
                            <input class="form-control" id="account-phone"
                                   placeholder="Account Phone" required="" type="current" value="{{$data["AccountDetail"]!=null?$data["AccountDetail"]->phone:"-"}}" readonly="true"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail">
                                Account Email
                            </label>
                            <input class="form-control" id="account-phone"
                                   placeholder="Account Email" required="" type="current" value="{{$data["AccountDetail"]!=null?$data["AccountDetail"]->email:"-"}}" readonly="true"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputBalance">
                                Account Balance
                            </label>
                            <input class="form-control" id="account-balance"
                                   placeholder="Account Balance" required="" type="current" value="{{$data["AccountBalance"]!=null?$data["AccountBalance"]->amount:"-"}}" readonly="true"/>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <section id="invoice">
                <div class="card alert">
                    <div class="card-header no-bg b-a-0">
                        <nav>
                            <li class="page-item" id="page-prev">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">«</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item" ><a class="page-link" href="#" id="page-no"><span>1</span></a></li>

                            <li class="page-item" id="page-next">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">»</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </nav>
                    </div>
                    <div class="card-block">
                        <table id="table-data" class="table table-bordered table-striped m-b-0">
                            <thead>
                            <tr>
                                <th>Item Type</th>
                                <th>Item Description</th>
                                <th>Item value</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
            </section>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function(){
//            Start Initialize form

            var page = 0 ;
            var size = 20 ;
            $('.select2').select2();

            $('#periode').datepicker( {
                format: "mmyyyy",
                viewMode: "months",
                minViewMode: "months"
            });

            $('#invoice-period').datepicker( {
                format: "mmyyyy",
                viewMode: "months",
                minViewMode: "months"
            });

            $('#invoice-duedate').datepicker( {
                format: "yyyy-mm-dd"
            });

//            end of Initialize form
            loadData(page,size) ;

        });

        function  loadData(page,size) {
            var id = "<?php echo $data["AccountBalance"]->accountId ?>" ;
//            alert(id);
            var url = "<?php echo url('/')?>/account-balance/history/"+id+"/" +page+"/"+size ;
//            alert(url);
            $.ajax({
                url: url,
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                //data: '{"foo":"bar"}',
                success: function (data) {
//                    alert(data);
                    tableData(data) ;
                },
                error: function(){
                    swal('Refresh', 'Failed load data!', 'warning');
                }
            });
        }

        function tableData(data){
            $("#table-data tbody").empty();
            $.each(data['content'],function(index,value){
                var tr0 = "<tr>";
                var td0 = "<td>"+value.itemType+"</td>";
                var td1 = "<td>"+value.itemDescription+"</td>";
                var td2 = "<td>"+value.itemValue+"</td>";
                var tr1 = "</tr>" ;

                var table = tr0 + td0 + td1 + td2 + tr1 ;
                $("#table-data tbody").append(table);

            });
        }

        $(document).delegate("#page-next","click",function(){
            var no = $("#page-no > span ").text() ;
            var next = parseInt(no) + 1 ;
            loadData(next - 1,20) ;
            $("#page-no > span ").text(next);
            return false ;
        });

        $(document).delegate("#page-prev","click",function(){
            var no = $("#page-no > span ").text() ;
            var prev = parseInt(no) - 1 ;
            loadData(prev - 1,20) ;
            $("#page-no > span ").text(prev);
            return false ;
        });


    </script>
@endsection