@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header no-bg b-a-0"></div>
                <div class="card-block">
                    <table id="table-popup" class="table table-bordered table-striped m-b-0">
                        <thead>
                        <tr>
                            <th>Period</th>
                            <th>Account Id</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                        <tr>
                            <td>{{$item['period']}}</td>
                            <td>{{$item['accountid']}}</td>
                            <td>{{$item['status']}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <br />
                    <div class="col-lg-12">
                        Note :
                        <p>
                            status <b>true</b> : success generate preparation <br/>
                            status <b>false</b> : already have invoice
                        </p>Message :
                        {{$result->message}}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('javascript')

@endsection