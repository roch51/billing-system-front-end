@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header no-bg b-a-0"></div>
                <div class="card-block"><form id="preparationform" data-toggle="validator" role="form">
                        <div class="input-prepend input-group m-b-1">
									  <span class="add-on input-group-addon">
                                          <a href="#" id="popup-account" data-toggle='modal' data-target='.bd-example-modal-lg'>
										    <i class="material-icons">
										     search
										    </i>
                                          </a>
									  </span>
                            <input id="account-id" name="account-id" class="form-control" value="" placeholder="Account Id" type="text" required="true" >
                            <input id="account-name" name="account-name" class="form-control" value="" placeholder="Full Name" type="text" required="true" readonly="true">

                        </div>
                        <button type="button" id="btn-search" class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                            <i class="material-icons">send</i>
                            <span>Search</span>
                        </button>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <section id="invoice">
                <div class="card alert">
                    <div class="card-header no-bg b-a-0">
                        <nav>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">«</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>

                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">»</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </nav>
                        <div class="alert alert-info" style="display:none">
                            ${message}
                        </div>
                    </div>
                    <div class="card-block">
                        <table id="table-data" class="table table-bordered table-striped m-b-0">
                            <thead>
                            <tr>
                                <th>Account Id</th>
                                <th>Full Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
            </section>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Select Account</h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <label for="account-fullname">Type Name</label>
                            <input class="form-control" id="account-fullname"
                                   placeholder="Enter name" required="true" />
                        </div>
                        <button type="button" id="btn-search-account" class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                            <i class="material-icons">send</i>
                            <span>Search</span>
                        </button>
                    </form>
                    <div class="card-block">
                        <table id="table-popup" class="table table-bordered table-striped m-b-0">
                            <thead>
                            <tr>
                                <th>Account Id</th>
                                <th>Full Name</th>
                                <th>Address</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function(){
//            Start Initialize form
            var page = 0 ;
            var size = 20 ;
            $('.select2').select2();

            $('#periode').datepicker( {
                format: "yyyymm",
                viewMode: "months",
                minViewMode: "months"
            });

            $('#invoice-period').datepicker( {
                format: "mmyyyy",
                viewMode: "months",
                minViewMode: "months"
            });

            $('#invoice-duedate').datepicker( {
                format: "yyyy-mm-dd",
            });

//            end of Initialize form

            $("#btn-search-account").click(function(){
                var fullname = $("#account-fullname").val() ;

                loadDataPopup(fullname,page,size) ;
                return false ;
            });

            $("#btn-search").click(function(){
                var accountId = $("#account-id").val() ;
                var generateAccountId = '{{$generateNo}}' ;
                var urls = "<?php echo url('/')?>/preparation/generate-account/"+generateAccountId+"/account/"+accountId ;

                $.ajax({
                    url:urls,
                    //beforeSend: function(xhr) {
                    //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                    //},
                    type: 'GET',
                    dataType: 'json',
                    contentType: 'application/json',
                    processData: false,
                    //data: '{"foo":"bar"}',
                    success: function (data) {
                        tableData(data) ;
                    },
                    error: function(){
                        swal('Cancelled', 'network error ! please refresh again', 'error');
                    }
                });

                return false ;
            });


            setTimeout(function(){
                //your code here
                loadData(page,size) ;

            }, 300);

            $("#account-id").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything

                if (e.which != 118 && e.which != 97 && e.which != 99){
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                }
            });

        });

        function  loadData(page,size) {
            var generateNo = "{{$generateNo}}" ;

            $.ajax({
                url: "<?php echo url('/')?>/preparation/after-generated/account/"+generateNo+"/"+page+"/"+size,
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                //data: '{"foo":"bar"}',
                success: function (data) {
                    tableData(data) ;
                },
                error: function(xhr,data){
                    swal('Internal Server Error', xhr.status, 'error');
                }
            });

        }

        function tableData(data){
            $("#table-data tbody").empty();
            $.each(data['content'],function(index,value){
                var tr0 = "<tr>";
                var td0 = "<td>"+value.accountId+"</td>";
                var td1 = "<td>"+value.fullname+"</td>";
                var optSelectedCandidate = "" ;
                var optSelectedWhitelist = "" ;
                if (value.statusDocumentId == 4) {
                    optSelectedCandidate = "selected";
                }

                if (value.statusDocumentId == 5) {
                    optSelectedWhitelist = "selected";
                }

                var td2 = "<td><select id='"+value.id+"' class='form-control opt-status' {{$status}}>" +
                    "<option value = '0'>--</option>" +
                    "<option value = '4' "+optSelectedCandidate+">Candidate</option>" +
                    "<option value = '5' "+optSelectedWhitelist+">Whitelist</option>" +
                    "</select></td>";
                var td3 = "<td><button id='"+value.id+"' class='btn btn-detail btn-outline-primary btn-sm'>Detail</button></td>";
                var tr1 = "</tr>" ;

                var table = tr0 + td0 + td1 + td2 +  td3 + tr1 ;
                $("#table-data tbody").append(table);

            });
        }

        $(document).delegate(".btn-gen-invoice","click",function(){
            var generateNo = this.id ;
            $("#invoice-generateno").val(generateNo) ;
            return false ;
        });


        $(document).delegate(".btn-detail","click",function () {
            var id = this.id ;
            window.location.replace("<?php echo url('/')?>/preparation/after-generated/items/"+id);
            return false ;
        }) ;

        function loadDataPopup(fullname,page,size) {
            var urls = "<?php echo url('/')?>/preparation/account/find/"+fullname+"/"+page+"/"+size;

            $.ajax({
                url: urls,
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
//                headers : {
//                    'X-CSRF-TOKEN' : $("#toket").val()
//                },
                success: function (data) {
                    tableDataPopup(data) ;
                },
                error: function(){
                    swal('Cancelled', 'Failed To Find!', 'error');
                }
            });
        }

        function tableDataPopup(data){
            $("#table-popup tbody").empty();
            $.each(data['content'],function(index,value){
                var tr0 = "<tr>";
                var td0 = "<td>"+value.accountId+"</td>";
                var td1 = "<td>"+value.fullname+"</td>";
                var td2 = "<td>"+value.address+"</td>";
                var td3 = "<td>"+value.email+"</td>";
                var td5 = "<td>" +
                    "<button id='"+value.accountId+"-"+value.fullname+"' type='button' class='btn-select-account btn btn-outline-primary m-r-xs m-b-xs btn-sm'>Select</button>" +
                    "</td>";
                var tr1 = "</tr>" ;

                var table = tr0 + td0 + td1 + td2 + td3 +  td5 + tr1 ;
                $("#table-popup tbody").append(table);

            });
        }

        $(document).delegate(".btn-select-account","click",function(){
            var account = this.id ;
            var detail = account.split("-") ;

            $("#account-id").val(detail[0]) ;
            $("#account-name").val(detail[1]) ;
            return false ;
        });

        $(document).delegate("#account-id","change",function () {

            var accountId = $("#account-id").val() ;
            var urls = "<?php echo url('/')?>/account/"+accountId ;
            if (accountId != "") {
                $.ajax({
                    url: urls,
                    //beforeSend: function(xhr) {
                    //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                    //},
                    type: 'GET',
                    dataType: 'json',
                    contentType: 'application/json',
                    processData: false,
                    //data: '{"foo":"bar"}',
                    success: function (data) {
                        if (data != null) {
                            $("#account-name").val(data.fullname) ;
                            $("#btn-generate").prop("disabled",false);
                        }
                    },
                    error: function(xhr,status,error){
                        swal("404", "No Detail For :"+accountId, "warning");

                        $("#account-name").val("") ;
                        $("#btn-generate").prop("disabled",true);
                    }
                });

            }
        }) ;

        $(document).delegate(".opt-status","change",function(){
            var status = $('.opt-status option:selected').val() ;
            var id = $(this).prop("id") ;
            var urls = "<?php echo url('/')?>/preparation/status/change";

            if (status == 0){
            }else {
                $.ajax({
                    url: urls,
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                    },
                    type: 'POST',
                    dataType: 'json',
                    processData: true,
                    data: {
                        id : id,
                        statusDocumentId : status
                    },
                    success: function (data) {
                        if (data != null) {
                            swal(data.status, data.message, "success");
                        }
                    },
                    error: function(xhr,status,error){
                        swal("404", error, "warning");

                        console.log(error+"-"+status);

                    }
                });
            }
            return false ;
        });
    </script>
@endsection