@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header no-bg b-a-0"></div>
                <div class="card-block"><form id="preparationform" data-toggle="validator" role="form">
                        <div class="input-prepend input-group m-b-1">
									  <span class="add-on input-group-addon">
										    <i class="material-icons">
										     date_range
										    </i>
									  </span>
                            <input id="periode" name="periode" class="form-control" value="" placeholder="Periode" type="text" required="true">

                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12">
                                <select id="billingcycle" data-placeholder="Pick Your Billing Cycle" class="select2 m-b-1" style="width: 100%;" required="true">
                                    @foreach($mbillingCycles as $bc)
                                        <option value="{{$bc->id}}">{{$bc->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button type="button" id="btn-generate" class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                            <i class="material-icons">send</i>
                            <span>Process</span>
                        </button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function(){
//            Start Initialize form
            var page = 0 ;
            var size = 20 ;
            $('.select2').select2();

            $('#periode').datepicker( {
                format: "yyyymm",
                viewMode: "months",
                minViewMode: "months"
            });

//            end of Initialize form
            //loadData(page,size) ;


        });

        function  loadData(page,size) {
            $.ajax({
                url: "preparation/rest/"+page+"/"+size,
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                //data: '{"foo":"bar"}',
                success: function (data) {
                    tableData(data) ;
                },
                error: function(){
                    swal('Cancelled', 'Failed To Generate!', 'error');
                }
            });
        }

        function tableData(data){
            $("#table-data tbody").empty();
            $.each(data['content'],function(index,value){
                var tr0 = "<tr>";
                var td0 = "<td>"+value.id+"</td>";
                var td1 = "<td>"+value.periode+"</td>";
                var td2 = "<td>"+value.billingCycle+"</td>";
                var td3 = "<td>"+value.generateNo+"</td>";
                var td5 = "<td>" +
                    "<button id='"+value.generateNo+"' type='button' class='btn-detail btn btn-outline-primary m-r-xs m-b-xs btn-sm'>Detail</button>" +
                    "<button id='"+value.generateNo+"' type='button' class='btn-invoice btn btn-outline-primary m-r-xs m-b-xs btn-sm'>Invoicing</button>" +
                    "</td>";
                var tr1 = "</tr>" ;

                var table = tr0 + td0 + td1 + td2 + td3 +  td5 + tr1 ;
                $("#table-data tbody").append(table);

            });
        }

        $(document).delegate(".btn-detail","click",function(){
            var generateNo = this.id ;
            window.location.replace("invoice/invoice-generated/"+generateNo);
            return false ;
        });

        $(document).delegate(".btn-invoice","click",function () {
            var generateNo = this.id ;
            alert(generateNo) ;
            return false ;
        });

        $(document).delegate("#btn-generate","click",function () {
            var periode = $("#periode").val() ;
            var billingcycle = $("#billingcycle").val() ;

            if (periode == "") {
                alert("Periode field is required !!") ;
                return false ;
            }

            if  (billingcycle == "") {
                alert("Billing Cycle field is required !!") ;
                return false ;
            }

            var path = "preparation/rest/check/"+periode+"/"+billingcycle ;

            $.ajax({
                url: path,
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                //data: '{"foo":"bar"}',
                success: function (data) {

                    if (data.status == "true"){
                        var urls = "<?php echo url('/')?>/preparation/generate-data/"+periode+"/"+billingcycle ;
                        setTimeout(function() {
                            swal({
                                title: 'Data Preparation',
                                text: 'Data CRM Period : '+periode+'\n Billing Cycle Id:'+billingcycle,
                                type: 'info',
                                showCancelButton: true,
                                closeOnConfirm: false,
                                showLoaderOnConfirm: true
                            }, function() {
                                $.ajax({
                                    url: urls,
                                    //beforeSend: function(xhr) {
                                    //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                                    //},
                                    type: 'GET',
                                    dataType: 'json',
                                    contentType: 'application/json',
                                    processData: false,
                                    //data: '{"foo":"bar"}',
                                    success: function (data) {
                                        setTimeout(function() {
                                            swal(data.message,data.status,'success');
                                        }, 2000);
                                    },
                                    error: function(data){
                                        swal(data.message, data.status, 'error');
                                    }
                                });
                            });
                        }, 2000);
                    }else {
                        swal('warning', 'Already Generated !!', 'warning');
                    }
                },
                error: function(){
                    swal('Cancelled', 'Failed !', 'error');
                }
            });
            return false ;
        });
    </script>
@endsection