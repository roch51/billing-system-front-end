@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header no-bg b-a-0"></div>
                <div class="card-block"><form id="preparationform" data-toggle="validator" role="form">
                        <div class="input-prepend input-group m-b-1">
									  <span class="add-on input-group-addon">
										    <i class="material-icons">
										     date_range
										    </i>
									  </span>
                            <input id="periode" name="periode" class="form-control" value="" placeholder="Periode" type="text" required="true">

                        </div>
                        <div class="input-prepend input-group m-b-1">
									  <span class="add-on input-group-addon">
                                          <a href="#" id="popup-account" data-toggle='modal' data-target='.bd-example-modal-lg'>
										    <i class="material-icons">
										     add
										    </i>
                                          </a>
									  </span>
                            <input id="account-id" name="account-id" class="form-control" value="" placeholder="Account Id" type="text" required="true" >
                            <input id="account-name" name="account-name" class="form-control" value="" placeholder="Full Name" type="text" required="true" readonly="true">

                        </div>
                        <button type="button" id="btn-add-temp" class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                            <i class="material-icons">add</i>
                            <span>add first bill temporary</span>
                        </button>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <section id="invoice">
                <div class="card alert">
                    <div class="card-header no-bg b-a-0">
                        <nav>
                            <button type="button" id="btn-generate" class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                                <i class="material-icons">send</i>
                                <span>Generate</span>
                            </button>

                            <button type="button" id="btn-delete" class="btn btn-danger btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                                <i class="material-icons">delete</i>
                                <span>Cancel</span>
                            </button>
                        </nav>
                    </div>
                    <div class="card-block">
                        <table id="table-data" class="table table-bordered table-striped m-b-0">
                            <thead>
                            <tr>
                                <th>Period</th>
                                <th>Account Id</th>
                                <th>Full Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
            </section>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Select Account</h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <label for="account-fullname">Type Name</label>
                            <input class="form-control" id="account-fullname"
                                   placeholder="Enter name" required="true" />
                        </div>
                        <button type="button" id="btn-search-account" class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                            <i class="material-icons">send</i>
                            <span>Search</span>
                        </button>
                    </form>
                    <div class="card-block">
                        <table id="table-popup" class="table table-bordered table-striped m-b-0">
                            <thead>
                            <tr>
                                <th>Account Id</th>
                                <th>Full Name</th>
                                <th>Address</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function(){
            $('#periode').datepicker( {
                format: "yyyymm",
                viewMode: "months",
                minViewMode: "months"
            });

            $("#account-id").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything

                if (e.which != 118 && e.which != 97 && e.which != 99){
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                }
            });

            setTimeout(function() {
                loadData() ;
            }, 1000);
        });

        $(document).delegate("#btn-search-account","click",function () {
            var fullname = $("#account-fullname").val() ;
            var page = 0 ;
            var size = 20 ;

            loadDataPopup(fullname,page,size) ;
            return false ;
        }) ;

        function loadDataPopup(fullname,page,size) {
            var urls = "<?php echo url('/')?>/preparation/account/find/"+fullname+"/"+page+"/"+size;

            $.ajax({
                url: urls,
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
//                headers : {
//                    'X-CSRF-TOKEN' : $("#toket").val()
//                },
                success: function (data) {
                    tableDataPopup(data) ;
                },
                error: function(){
                    swal('Cancelled', 'Failed To Find!', 'error');
                }
            });
        }

        function tableDataPopup(data){
            $("#table-popup tbody").empty();
            $.each(data['content'],function(index,value){
                var tr0 = "<tr>";
                var td0 = "<td>"+value.accountid+"</td>";
                var td1 = "<td>"+value.fullname+"</td>";
                var td2 = "<td>"+value.address+"</td>";
                var td3 = "<td>"+value.email+"</td>";
                var td5 = "<td>" +
                    "<button id='"+value.accountid+"-"+value.fullname+"' type='button' class='btn-select-account btn btn-outline-primary m-r-xs m-b-xs btn-sm'>Select</button>" +
                    "</td>";
                var tr1 = "</tr>" ;

                var table = tr0 + td0 + td1 + td2 + td3 +  td5 + tr1 ;
                $("#table-popup tbody").append(table);

            });
        }

        $(document).delegate(".btn-select-account","click",function(){
            var account = this.id ;
            var detail = account.split("-") ;

            $("#account-id").val(detail[0]) ;
            $("#account-name").val(detail[1]) ;
            return false ;
        });

        $(document).delegate("#btn-add-temp","click",function () {
           var acccountId = $("#account-id").val() ;
           var period = $("#periode").val() ;


            if (period == ''){
                swal("", "period is required", "warning");
                return false ;
            }

            if (acccountId == ''){
                swal("", "account id is required", "warning");
                return false ;
            }


            var urls = "<?php echo url('/')?>/preparation/firstbill/temp/"+acccountId+"/"+period;

            $.ajax({
                url: urls,
                type: 'GET',
                dataType: 'json',
                success: function (data,status,xhr) {
                    swal(xhr.status.toString(), xhr.statusText.toString(), "success");
                    loadData();
                },
                error: function(xhr,status,error){
                    swal("404", "please do first payment", "warning");
                    loadData();

                }
            });

           return false ;
        });

        function tableData(data){
            $("#table-data tbody").empty();
            $.each(data['content'],function(index,value){
                var tr0 = "<tr>";
                var td0 = "<td>"+value.period+"</td>";
                var td1 = "<td>"+value.accountId+"</td>";
                var td2 = "<td>"+value.fullname+"</td>";
                var td3 = "<td>" +
                    "<button id='"+value.accountId+"' type='button' class='btn-del-account btn btn-danger btn-icon loading-demo m-r-xs m-b-xs btn-sm'>" +
                    "<i class='material-icons'>delete</i><span>delete</span>" +
                    "</button>" +
                    "</td>";
                var tr1 = "</tr>" ;

                var table = tr0 + td0 + td1 + td2 + td3 + tr1 ;
                $("#table-data tbody").append(table);

            });
        }

        function  loadData() {

            $("#periode").val('') ;
            $("#account-id").val('') ;
            $("#account-name").val('') ;

            $.ajax({
                url: "<?php echo url('/')?>/preparation/firstbill/tmplist",
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                //data: '{"foo":"bar"}',
                success: function (data) {
                    tableData(data) ;
                },
                error: function(){
                    swal('500', "Internal Server Error", 'error');
                }
            });

        }

        $(document).delegate("#btn-generate","click",function () {
            var period = $("#periode").val() ;

            if (period == '') {
                swal("", "period is required", "warning");
                return false ;
            }

            var urls = "<?php echo url('/')?>/preparation/firstbill/generate/"+period;

            swal({
                title: 'Generate First Bill',
                text: 'period: '+period,
                type: 'info',
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function() {
                $.ajax({
                    url: urls,
                    //beforeSend: function(xhr) {
                    //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                    //},
                    type: 'GET',
                    dataType: 'json',
                    contentType: 'application/json',
                    processData: false,
                    //data: '{"foo":"bar"}',
                    success: function (data) {
                        console.log(data) ;
                        setTimeout(function() {
                            swal(data.message,data.status,'success');
                            loadData() ;
                        }, 2000);
                    },
                    error: function(data){
                        swal(data.message, data.status, 'error');
                    }
                });
            });

        });

        $(document).delegate("#btn-delete","click",function () {
            var urls = "<?php echo url('/')?>/preparation/firstbill/delete";

            swal({
                title: 'Cancel',
                text: 'Delete all account in first bill temporary',
                type: 'info',
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function() {
                $.ajax({
                    url: urls,
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                    },
                    type: 'DELETE',
                    dataType: 'json',
                    contentType: 'application/json',
                    processData: false,
                    //data: '{"foo":"bar"}',
                    success: function (data,status,xhr) {
                        console.log(status) ;

                        setTimeout(function() {
                            swal(xhr.status.toString(), "Delete Success", "success");
                            loadData() ;
                        }, 2000);
                    },
                    error: function(data,status,xhr){
//                        swal(xhr.status.toString(), xhr.statusText.toString(), "error");
                        console.log(status) ;
                        loadData() ;

                    }
                });
            });
        });

        $(document).delegate(".btn-del-account","click",function () {
            var accountId = this.id ;
            var pathDelAcc = "<?php echo url('/')?>/preparation/firstbill/delete/"+accountId;

            swal({
                title: 'Delete',
                text: 'Delete account in first bill temporary : '+accountId,
                type: 'info',
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function() {
                $.ajax({
                    url: pathDelAcc,
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                    },
                    type: 'DELETE',
                    dataType: 'json',
                    //data: '{"foo":"bar"}',
                    success: function (data,status,xhr) {
                        setTimeout(function() {
                            console.log(status) ;
                            swal(xhr.status.toString(), "Delete Success", "success");
                            loadData() ;
                        }, 2000);
                    },
                    error: function(data,status,xhr){
                        console.log(status) ;
                        swal(xhr.status.toString(), xhr.statusText.toString(), "error");
                        loadData() ;
                    }
                });
            });
        });

        $(document).delegate("#account-id","change",function () {

            var accountId = $("#account-id").val() ;
            var urls = "<?php echo url('/')?>/account/"+accountId ;
            if (accountId != "") {
                $.ajax({
                    url: urls,
                    //beforeSend: function(xhr) {
                    //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                    //},
                    type: 'GET',
                    dataType: 'json',
                    //data: '{"foo":"bar"}',
                    success: function (data) {
                        if (data != null) {
                            $("#account-name").val(data.fullname) ;
                            $("#btn-generate").prop("disabled",false);
                        }
                    },
                    error: function(xhr,status,error){
                        swal("404", "No Detail For :"+accountId, "warning");

                        $("#account-name").val("") ;
                        $("#btn-generate").prop("disabled",true);
                    }
                });

            }
        }) ;
    </script>
@endsection