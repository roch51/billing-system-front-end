@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section id="invoice">
                <div class="card alert">
                    <div class="card-header no-bg b-a-0">
                        <nav>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">«</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>

                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">»</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </nav>
                        <div class="alert alert-info" style="display:none">
                            ${message}
                        </div>
                    </div>
                    <div class="card-block">
                        <table id="table-data" class="table table-bordered table-striped m-b-0">
                            <thead>
                            <tr>
                                <th>Item Type</th>
                                <th>Item Description</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data['items'] as $item)
                            <tr>
                                <td>{{$item['itemType']}}</td>
                                <td>{{$item['itemDescription']}}</td>
                                <td>{{$item['itemValue']}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </section>
        </div>
    </div>

@endsection

@section('javascript')
    <script>

    </script>
@endsection