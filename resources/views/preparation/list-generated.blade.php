@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header no-bg b-a-0"></div>
                <div class="card-block"><form id="preparationform" data-toggle="validator" role="form">
                        <div class="input-prepend input-group m-b-1">
									  <span class="add-on input-group-addon">
										    <i class="material-icons">
										     date_range
										    </i>
									  </span>
                            <input id="periode" name="periode" class="form-control" value="" placeholder="Periode" type="text" required="true">

                        </div>
                        <button type="button" id="btn-search" class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                            <i class="material-icons">send</i>
                            <span>Search</span>
                        </button>
                    </form>

                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <section id="invoice">
                <div class="card alert">
                    <div class="card-header no-bg b-a-0">
                        <nav>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">«</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>

                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">»</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </nav>
                        <div class="alert alert-info" style="display:none">
                            ${message}
                        </div>
                    </div>
                    <div class="card-block">
                        <table id="table-data" class="table table-bordered table-striped m-b-0">
                            <thead>
                            <tr>
                                <th>Period</th>
                                <th>Generate No</th>
                                <th>Generate Type</th>
                                <th>Status</th>
                                <th>Total Account</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
            </section>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function(){
//            Start Initialize form
            var page = 0 ;
            var size = 20 ;
            $('.select2').select2();

            $('#periode').datepicker( {
                format: "yyyymm",
                viewMode: "months",
                minViewMode: "months"
            });

//            end of Initialize form

            setTimeout(function() {
                loadData(page,size) ;
            }, 1000);



        });

        function  loadData(page,size) {
            var periode = '2' ;
            var urls = "<?php echo url('/')?>/preparation/after-generated/"+periode+"/"+page+"/"+size ;
            $.ajax({
                url: urls,
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                //data: '{"foo":"bar"}',
                success: function (data) {
                    tableData(data) ;
                },
                error: function(){
                    swal('Cancelled', 'Failed To Generate!', 'error');
                }
            });
        }

        function tableData(data){
            $("#table-data tbody").empty();
            $.each(data['content'],function(index,value){
                var tr0 = "<tr>";
                var td0 = "<td>"+value.periode+"</td>";
                var td1 = "<td>"+value.generateNo+"</td>";
                var td2 = "<td>"+value.generateType+"</td>";
                var td3 = "<td>"+value.status+"</td>";
                var td4 = "<td>"+value.totalAccounts+"</td>";
                var td5 = "<td>" +
                    "<button id='"+value.generateNo+"-"+value.status+"' type='button' class='btn-detail btn btn-outline-primary m-r-xs m-b-xs btn-sm'>Detail</button>" +
                    "</td>";
                var tr1 = "</tr>" ;

                var table = tr0 + td0 + td1 + td2 + td3 + td4 + td5 + tr1 ;
                $("#table-data tbody").append(table);

            });
        }

        $(document).delegate(".btn-detail","click",function(){
            var data = this.id.split("-") ;
            var generateNo = data[0] ;
            var status = data[1] ;

            var urls = "<?php echo url('/')?>/preparation/detail/"+generateNo+"/"+status ;

            window.location = urls;
            return false ;
        });

        $(document).delegate(".btn-invoice","click",function () {
            var generateNo = this.id ;
            alert(generateNo) ;
            return false ;
        });

        $(document).delegate("#btn-search","click",function () {
            var periode = $("#periode").val() ;
            var page = 0 ;
            var size = 20 ;

            if (periode == "") {
                alert("Periode field is required !!") ;
                return false ;
            }

            var path = "<?php echo url('/')?>/preparation/after-generated/"+periode+"/"+page+"/"+size ;
            $.ajax({
                url: path,
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                //data: '{"foo":"bar"}',
                success: function (data) {
                    tableData(data);
                },
                error: function(){
                    swal('Cancel', 'Not Found !', 'warning');
                }
            });
            return false ;
        });
    </script>
@endsection