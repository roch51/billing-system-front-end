@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <!-- BEGIN STEP FORM WIZARD -->
            <div class="tsf-wizard tsf-wizard-1">
                <!-- BEGIN NAV STEP-->
                <div class="tsf-nav-step">
                    <!-- BEGIN STEP INDICATOR-->
                    <ul class="gsi-step-indicator triangle gsi-style-1 gsi-transition ">
                        <li class="current" data-target="step-1">
                            <a href="javascript:;">
                                <div class="number">1</div>
                                <div class="desc">
                                    <label>
                                        CRM
                                    </label>
                                    <span>
                                Collect data
                              </span>
                                </div>
                            </a>
                        </li>
                        <li data-target="step-2">
                            <a href="javascript:;">
                                <div class="number">2</div>
                                <div class="desc">
                                    <label>
                                        Sync BSS1 Payment
                                    </label>
                                    <span>
                                First Bill
                              </span>
                                </div>
                            </a>
                        </li>
                        <li class="current" data-target="step-3">
                            <a href="javascript:;">
                                <div class="number">3</div>
                                <div class="desc">
                                    <label>
                                        Sync BSS1 Payment
                                    </label>
                                    <span>
                                Previous Payment
                              </span>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- END STEP INDICATOR -->
                </div>
                <!-- END NAV STEP-->
                <!-- BEGIN STEP CONTAINER -->
                <div class="tsf-container">
                    <!-- BEGIN CONTENT-->
                    <form class="tsf-content">
                        <!-- BEGIN STEP 1-->
                        <!-- BEGIN STEP 1-->
                        <div class=" tsf-step step-1 active">
                            <fieldset>
                                <legend>
                                    Sync Data CRM
                                </legend>
                                <!-- BEGIN STEP CONTENT-->
                                <div class="tsf-step-content">
                                    <div class="col-lg-12">
                                        Collect CRM data period : {{$periode}} and billing cycle {{$bc}}
                                        <br /><br />
                                        <button type="button" id="btn-generate" class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                                            <i class="material-icons">send</i>
                                            <span>Collect CRM's data</span>
                                        </button>
                                    </div>
                                </div>
                                <!-- END STEP CONTENT-->
                            </fieldset>
                        </div>
                        <!-- END STEP 1-->
                        <div class="tsf-step step-2">
                            <fieldset>
                                <legend>
                                    Provide previous first bill
                                </legend>
                                <div class="row">
                                    <!-- BEGIN STEP CONTENT-->
                                    <div class="tsf-step-content">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">
                                                    Current Periode
                                                </label>
                                                <input class="form-control" id="curr_periode_fb"
                                                       placeholder="Enter current periode" required="" type="current" value="{{$periode}}" readonly="true"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputFrom">
                                                    Date Range
                                                </label>
                                                <div class="input-prepend input-group m-b-1">
                                                <span class="add-on input-group-addon">
                                                    <i class="material-icons">
                                                      date_range
                                                    </i>
                                                </span>
                                                <input type="text" name="date-range" id="date-range-fb" class="form-control drp" value="" placeholder="Date range picker"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                           This process is required to collect first bill information in the previous payment
                                            <br /><br />
                                            <button type="button" id="btn-collect-fb" class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                                                <i class="material-icons">send</i>
                                                <span>Collect data</span>
                                            </button>
                                        </div>

                                    </div>
                                    <!-- END STEP CONTENT-->
                                </div>
                            </fieldset>
                        </div>
                        <!-- END STEP 2-->
                        <!-- BEGIN STEP 3-->
                        <div class=" tsf-step step-3 ">
                            <fieldset>
                                <legend>
                                    Provide your previous payment
                                </legend>
                                <!-- BEGIN STEP CONTENT-->
                                <div class="tsf-step-content">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">
                                            Current Periode
                                        </label>
                                        <input class="form-control" id="curr_periode"
                                               placeholder="Enter current periode" required="" type="current" value="{{$periode}}" readonly="true"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputFrom">
                                            Date Range
                                        </label>
                                        <div class="input-prepend input-group m-b-1">
                                                <span class="add-on input-group-addon">
                                                    <i class="material-icons">
                                                      date_range
                                                    </i>
                                                </span>
                                            <input type="text" name="date-range" id="date-range" class="form-control drp" value="" placeholder="Date range picker"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        This process is required to collect payment information in the previous payment
                                        <br /><br />
                                        <button type="button" id="btn-collect" class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                                            <i class="material-icons">send</i>
                                            <span>Collect data</span>
                                        </button>
                                    </div>
                                </div>
                                <!-- END STEP CONTENT-->
                            </fieldset>
                        </div>
                        <!-- END STEP 3-->
                    </form>
                    <!-- END CONTENT-->
                    <!-- BEGIN CONTROLS-->
                    <div class="tsf-controls ">
                        <!-- BEGIN PREV BUTTTON-->
                        <button id="btn-prev" class="btn btn-primary btn-icon btn-left tsf-wizard-btn" data-type="prev" type="button">
                            <i class="material-icons">arrow_back</i>
                            <span>Prev</span>
                        </button>
                        <!-- END PREV BUTTTON-->
                        <!-- BEGIN NEXT BUTTTON-->
                        <button id="btn-next" class="btn btn-primary btn-icon btn-right tsf-wizard-btn" data-type="next"
                                type="button">
                            <i class="material-icons">arrow_forward</i>
                            <span>Next</span>
                        </button>
                        <!-- END NEXT BUTTTON-->
                        <!-- BEGIN FINISH BUTTTON-->
                        <button class="btn btn-right tsf-wizard-btn" data-type="finish" type="button" id="finish">
                            FINISH
                        </button>
                        <!-- END FINISH BUTTTON-->
                    </div>
                    <!-- END CONTROLS-->
                </div>
                <!-- END STEP CONTAINER -->
            </div>
            <!-- END STEP FORM WIZARD -->
        </div>
    </div>
@endsection

@section('javascript')
   <script>
       $("document").ready(function(){
           var tsf_style = 'style3';
           var tsf_markup = '';

           $(function() {
               tsf_markup = $('.wizards').innerHTML;
               pageLoadScript();
           });

           function mreload() {
               $('.wizards').html('');
               $('.wizards').append(tsf_markup).html()
               pageLoadScript();
           }

           function pageLoadScript() {


               var tsf_stepEffect = $('.wizard-effect').val();
               var tsf_stepTransition = 1 ;
               var tsf_showButtons = 1 ;
               var tsf_showStepNum = 1;
               $('.tsf-wizard-1').tsfWizard({
                   stepEffect: tsf_stepEffect,
                   stepStyle: tsf_style,
                   navPosition: 'top',
                   stepTransition: tsf_stepTransition,
                   showButtons: tsf_showButtons,
                   showStepNum: tsf_showStepNum,
                   prevBtn: '<i class="material-icons">arrow_back</i> Prev',
                   nextBtn: 'Next <i class="material-icons">arrow_forward</i>',
                   finishBtn: 'Finish'
               });
           }

           $('#date-range-fb').daterangepicker({
               timePicker: false,
               timePickerIncrement: 30,
               locale: {
                   format: 'YYYY/MM/DD'
//                   format: 'MM/DD/YYYY h:mm A'
               }
           });
           $('#date-range').daterangepicker({
               timePicker: false,
               timePickerIncrement: 30,
               locale: {
                   format: 'YYYY/MM/DD'
//                   format: 'MM/DD/YYYY h:mm A'
               }
           });

           $('#due-date').datepicker( {
               format: "yyyy-mm-dd",
           });
       });

       $(document).delegate("#btn-collect-fb","click",function () {
           var daterange = $("#date-range").val().split("-") ;
           var from = (daterange[0].replace(/\s+/g, '')).replace(new RegExp('/', 'g'),'-') ;
           var to = (daterange[1].replace(/\s+/g, '')).replace(new RegExp('/', 'g'),'-') ;
           var urls = "<?php echo url('/')?>/preparation/firstbill/"+from+"/"+to ;

           swal({
               title: 'Sync First Bill BSS1',
               text: 'Previous payment : '+daterange,
               type: 'info',
               showCancelButton: true,
               closeOnConfirm: false,
               showLoaderOnConfirm: true
           }, function() {
               $.ajax({
                   url: urls,
                   //beforeSend: function(xhr) {
                   //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                   //},
                   type: 'GET',
                   dataType: 'json',
                   contentType: 'application/json',
                   processData: false,
                   //data: '{"foo":"bar"}',
                   success: function (data) {
                       setTimeout(function() {
                           swal(data.message,data.status,'success');
                       }, 2000);
                   },
                   error: function(data){
                       swal(data.message, data.status, 'error');
                   }
               });
           });
       });

       $(document).delegate("#btn-collect","click",function () {
           var daterange = $("#date-range").val().split("-") ;
           var from = (daterange[0].replace(/\s+/g, '')).replace(new RegExp('/', 'g'),'-') ;
           var to = (daterange[1].replace(/\s+/g, '')).replace(new RegExp('/', 'g'),'-') ;
           var urls = "<?php echo url('/')?>/preparation/invoicepayment/"+from+"/"+to ;

           swal({
               title: 'Sync Prev Payment BSS1',
               text: 'Previous payment : '+prevPeriode,
               type: 'info',
               showCancelButton: true,
               closeOnConfirm: false,
               showLoaderOnConfirm: true
           }, function() {
               $.ajax({
                   url: urls,
                   //beforeSend: function(xhr) {
                   //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                   //},
                   type: 'GET',
                   dataType: 'json',
                   contentType: 'application/json',
                   processData: false,
                   //data: '{"foo":"bar"}',
                   success: function (data) {
                       setTimeout(function() {
                           swal(data.message,data.status,'success');
                       }, 2000);
                   },
                   error: function(data){
                       swal(data.message, data.status, 'error');
                   }
               });
           });
       });

       $(document).delegate("#btn-generate","click",function () {
           var bcId = "{{$bc}}" ;
           var periode = "{{$periode}}" ;
           var urls = "<?php echo url('/')?>/preparation/generate-data/"+periode+"/"+bcId ;
           swal({
               title: 'Data Preparation',
               text: 'Data CRM Period : '+periode+'\n Billing Cycle Id:'+bcId,
               type: 'info',
               showCancelButton: true,
               closeOnConfirm: false,
               showLoaderOnConfirm: true
           }, function() {
               $.ajax({
                   url: urls,
                   //beforeSend: function(xhr) {
                   //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                   //},
                   type: 'GET',
                   dataType: 'json',
                   contentType: 'application/json',
                   processData: false,
                   //data: '{"foo":"bar"}',
                   success: function (data) {
                       setTimeout(function() {
                           swal(data.message,data.status,'success');
                       }, 2000);
                   },
                   error: function(data){
                       swal(data.message, data.status, 'error');
                   }
               });
           });
           return false ;
       }) ;

       $(document).delegate("#finish","click",function () {
           var urls = "<?php echo url('/')?>/invoice" ;
           window.location.replace(urls);
           return false ;
       });
   </script>
@endsection