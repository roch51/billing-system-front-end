@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="card profile-bio">
                <a class="background">
                </a>
                <a href="javascript:;" class="avatar">
                    <img src="{{ asset('images/avatar.jpg') }}" alt=""/>
                </a>
                <div class="user-details">
                    <div class="user-name">
                        <a href="javascript:;">
                            {{$data['AccountDetail']['fullname']}}
                        </a>
                    </div>
                </div>
                <div class="user-stats">
                    <ul>
                        <li>
                            <a href="javascript:;">
                          <span class="small text-uppercase block text-muted">
                            <strong>

                            </strong>
                          </span>
                                <h5 class="m-b-0">
                                    <strong>

                                    </strong>
                                </h5>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;">
                          <span class="small text-uppercase block text-muted">
                            <strong>

                            </strong>
                          </span>
                                <h5 class="m-b-0">
                                    <strong>

                                    </strong>
                                </h5>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;">
                          <span class="small text-uppercase block text-muted">
                            <strong>

                            </strong>
                          </span>
                                <h5 class="m-b-0">
                                    <strong>

                                    </strong>
                                </h5>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="profile-sidebar">
                <div class="card">
                    <div class="card-block">
                        <small class="bold text-uppercase">
                            Address :
                        </small>
                        <p class="m-b-0 text-uppercase">
                            {{$data['AccountDetail']['address']}}
                        </p>
                    </div>
                    <div class="card-block">
                        <small class="bold text-uppercase">
                            Email :
                        </small>
                        <p class="m-b-0 text-uppercase">
                            {{$data['AccountDetail']['email']}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#one" role="tab">Invoice Payment</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#two" role="tab">Payment Reserve</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#three" role="tab">Adjustment</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#four" role="tab">Invoice</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="one" role="tabpanel">
                    <div class="card-block">
                        <table id="table-data" class="table table-bordered table-striped m-b-0">
                            <thead>
                            <tr>
                                <th>Invoice Id</th>
                                <th>Invoice Period</th>
                                <th>Previous Balance</th>
                                <th>Current Balance</th>
                                <th>Amount</th>
                                <th>Paid</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data['InvoicePayments']['content'] as $invoicePayment)
                                <tr>
                                    <td>{{$invoicePayment['invoiceId']}}</td>
                                    <td>{{$invoicePayment['invoicePeriod']}}</td>
                                    <td>{{$invoicePayment['previousBalance']}}</td>
                                    <td>{{$invoicePayment['currentBalance']}}</td>
                                    <td>{{$invoicePayment['amount']}}</td>
                                    <td>{{$invoicePayment['paid']}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="two" role="tabpanel">
                    <div class="card-block">
                        <table id="table-data" class="table table-bordered table-striped m-b-0">
                            <thead>
                            <tr>
                                <th>Payment Date</th>
                                <th>Previous Balance</th>
                                <th>Current Balance</th>
                                <th>Amount</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data['PaymentReserves']['content'] as $paymentReserve)
                                <tr>
                                    <td>{{$paymentReserve['paymentDate']==null?0:$paymentReserve['paymentDate']}}</td>
                                    <td>{{$paymentReserve['previousBalance']==null?0:$paymentReserve['previousBalance']}}</td>
                                    <td>{{$paymentReserve['currentBalance']==null?0:$paymentReserve['currentBalance']}}</td>
                                    <td>{{$paymentReserve['amount']==null?0:$paymentReserve['amount']}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="three" role="tabpanel">
                    <div class="card-block">
                        <table id="table-data" class="table table-bordered table-striped m-b-0">
                            <thead>
                            <tr>
                                <th>Adjustment Date</th>
                                <th>Amount</th>
                                <th>Notes</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data['Adjustments']['content'] as $adjustment)
                                <tr>
                                    <td>{{$adjustment['adjustmentDate']==null?'-':$adjustment['adjustmentDate']}}</td>
                                    <td>{{$adjustment['amount']==null?0:$adjustment['amount']}}</td>
                                    <td>{{$adjustment['notes']==null?'-':$adjustment['notes']}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="four" role="tabpanel">
                    <div class="card-block">
                        <table id="table-data" class="table table-bordered table-striped m-b-0">
                            <thead>
                            <tr>
                                <th>Invoice No</th>
                                <th>Period</th>
                                <th>Invoice Date</th>
                                <th>Due Date</th>
                                <th>Last Balance</th>
                                <th>Current Balance</th>
                                <th>Total Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data['Invoices']['content'] as $invoice)
                                <tr>

                                    <td><a href="{{ url('/invoice/invoice-detail/'.$invoice['id']) }}"><i class="material-icons text-primary">link </i>{{$invoice['invoiceNo']==null?'-':$invoice['invoiceNo']}}</a></td>
                                    <td>{{$invoice['periode']==null?0:$invoice['periode']}}</td>
                                    <td>{{$invoice['invoiceDate']==null?'-':$invoice['invoiceDate']}}</td>
                                    <td>{{$invoice['dueDate']==null?'-':$invoice['dueDate']}}</td>
                                    <td>{{$invoice['lastBalance']==null?'-':$invoice['lastBalance']}}</td>
                                    <td>{{$invoice['currentBalance']==null?'-':$invoice['currentBalance']}}</td>
                                    <td>{{$invoice['totalAmount']==null?'-':$invoice['totalAmount']}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
    </script>
@endsection