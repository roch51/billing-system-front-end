@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @elseif(Session::has('warning'))
                <div class="alert alert-danger">
                    {{ Session::get('warning') }}
                </div>
            @else
            @endif
            <div class="card">
                <div class="card-header no-bg b-a-0"></div>
                <div class="card-block">
                    <form method="POST" action="/reversal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="account_id">
                                Account Id
                            </label>
                            <input class="form-control" id="account_id"
                                   name="data[account_id]" required="" type="current" value="">
                        </div>
                        <div class="form-group">
                            <label for="account_id">
                                Reference Id
                            </label>
                            <input class="form-control" id="reference_id"
                                   name="data[reference_id]" required="" type="current" value="">
                        </div>
                        <button type="submit" id="btn-generate"
                                class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                            <i class="material-icons">send</i>
                            <span>Save</span>
                        </button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function () {

            $('#periode').datepicker({
                format: "yyyymm",
                viewMode: "months",
                minViewMode: "months"
            });

        });


    </script>
@endsection