@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
            @elseif(Session::has('warning'))
                <div class="alert alert-danger">
                    {{ Session::get('warning') }}
                </div>
            @else
                @endif
            <div class="card">
                <div class="card-header no-bg b-a-0"></div>
                <div class="card-block">
                    <form method="POST" action="/adjustment" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="account_id">
                                Account Id
                            </label>
                            <input class="form-control" id="account_id"
                                   name="data[accountId]" required="" type="current" value="">
                            <input class="form-control" id="billingStatus"
                                   required="" type="hidden" value="0" name="data[billingStatus]">
                        </div>
                        <div class="form-group">
                            <label for="invoiceId">
                                Invoice
                            </label>
                            <input class="form-control " name="data[invoiceId]" id="invoiceId"
                                   required="" type="current" value="">
                        </div>
                        <div class="form-group">
                            <label for="payment_date">
                                Adjustment Date
                            </label>
                            <input class="form-control " name="data[adjustmentDate]" id="adjustmentDate"
                                   required="" type="current" value="">
                        </div>
                        <div class="form-group">
                            <label for="adjustmentReasonId">
                                Adjustment Reason
                            </label>
                            <select id="adjustmentReasonId" data-placeholder="Pick Your Adjustmen Reason" class="select2 m-b-1"
                                    style="width: 100%;" required="true" name="data[adjustmentReasonId]">
                                    <option value="1">Penyesuaian Billing</option>
                                    <option value="2">Restitution</option>
                                    <option value="3">Retention</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="Paid">
                                Paid
                            </label>
                            <input class="form-control " name="data[amount]" id="amount"
                                   required="" type="current" value="">
                        </div>
                        <div class="form-group">
                            <label for="notes">
                                Note
                            </label>
                            <textarea class="form-control " name="data[notes]" id="notes"
                                      required="" type="current" value=""></textarea>
                        </div>
                        <div class="form-group">
                            <label for="adjustmentType">
                                Adjustment Type
                            </label>
                            <select id="adjustmentType" data-placeholder="Pick Your Adjustmen Reason" class="select2 m-b-1"
                                    style="width: 100%;" required="true" name="data[adjustmentType]">
                                <option value="1">Debit</option>
                                <option value="2">Credit</option>
                            </select>
                        </div>
                        <button type="submit" id="btn-generate"
                                class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                            <i class="material-icons">send</i>
                            <span>Create</span>
                        </button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function () {

            $('.select2').select2();

            $('#adjustmentDate').datepicker({
                format: "yyyy-mm-dd",
                viewMode: "days",
                minViewMode: "days"
            });



        });


    </script>
@endsection