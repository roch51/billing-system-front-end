@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @elseif(Session::has('warning'))
                <div class="alert alert-danger">
                    {{ Session::get('warning') }}
                </div>
            @else
            @endif
            <div class="card">
                <div class="card-header no-bg b-a-0"></div>
                <div class="card-block">
                    <form method="POST" action="/payment" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input class="form-control" id="channel"
                               required="" type="hidden" value="Cash" name="data[channel]">
                        <input class="form-control" id="payment_channel_id"
                               required="" type="hidden" value="5" name="data[channel_id]">
                        <input class="form-control" id="payment_channel"
                               required="" type="hidden" value="Galeri" name="data[payment_channel]">
                        <input class="form-control" id="payment_channel_id"
                               required="" type="hidden" value="7" name="data[payment_channel_id]">
                        <input class="form-control" id="payment_channel_id"
                               required="" type="hidden" value="Galeri" name="data[sub_channel]">
                        <input class="form-control" id="payment_channel_id"
                               required="" type="hidden" value="7" name="data[sub_channel_id]">
                        <div class="form-group">
                            <label for="payment-type">
                                Payment Type
                            </label>

                            <select id="payment-type" data-placeholder="Pick Your Billing Cycle" class="select2 m-b-1"
                                    style="width: 100%;"
                                    required="true" name="data[payment_type]">
                                <option value="1">Billing Invoice</option>
                                <option value="2">Reserve</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="account_id">
                                Account Id
                            </label>
                            <input class="form-control" id="account_id"
                                   name="data[account_id]" required="" type="current" value="">
                        </div>
                        <div class="form-group">
                            <label for="payment_date">
                                Payment Date
                            </label>
                            <input class="form-control " name="data[payment_date]" id="payment_date"
                                   required="" type="current" value="">
                        </div>
                        <div class="form-group">
                            <label for="billingcycle">
                                Billing Cycle
                            </label>

                            <select id="billingcycle" data-placeholder="Pick Your Billing Cycle" class="select2 m-b-1"
                                    style="width: 100%;" required="true" name="data[billing_cycle_id]">
                                @foreach($mbillingCycles as $bc)
                                    <option value="{{$bc->id}}">{{$bc->billingCycle}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="paid">
                                Paid
                            </label>
                            <input class="form-control" id="paid"
                                   required="" type="current" value="" name="data[paid]">
                        </div>
                        <div class="form-group">
                            <label for="refference_id">
                                Reference Id
                            </label>
                            <input class="form-control" id="refference_id"
                                   required="" type="current" value="" name="data[refference_id]">
                        </div>
                        <button type="submit" id="btn-generate"
                                class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                            <i class="material-icons">send</i>
                            <span>Process</span>
                        </button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function () {
            $('#channel-id').change(function () {
                var id_channel = $(this).val();
                $('#payment_channel_id').val(id_channel);
                loadData(id_channel);
            });
            $('#sub_channel_id').change(function () {
                var id_channel_info = $(this).val();
                loadData2(id_channel_info);
            });

            $('.select2').select2();

            $('#payment_date').datepicker({
                format: "yyyy-mm-dd",
                viewMode: "days",
                minViewMode: "days"
            });

            function loadData(id_channel) {
                var url = "<?php echo url('/')?>/payment/channel/" + id_channel;
                $.ajax({
                    url: url,
                    //beforeSend: function(xhr) {
                    //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                    //},
                    type: 'GET',
                    dataType: 'json',
                    contentType: 'application/json',
                    processData: false,
                    //data: '{"foo":"bar"}',
                    success: function (data) {
                        $('#channel').val(data.name);
                        $('#payment_channel').val(data.name);
                    },
                    error: function () {
                        swal('Refresh', 'Failed load data!', 'warning');
                    }
                });
            }

            function loadData2(id_channel_info) {
                var url = "<?php echo url('/')?>/payment/sub-channel/" + id_channel_info;
                $.ajax({
                    url: url,
                    //beforeSend: function(xhr) {
                    //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                    //},
                    type: 'GET',
                    dataType: 'json',
                    contentType: 'application/json',
                    processData: false,
                    //data: '{"foo":"bar"}',
                    success: function (data) {
                        $('#sub_channel').val(data.name);
                    },
                    error: function () {
                        swal('Refresh', 'Failed load data!', 'warning');
                    }
                });
            }

        });


    </script>
@endsection