@extends('layouts.master')
@section('content')
    <div class="card">
        <div class="card-header no-bg b-a-0">
            Background Process
        </div>
        <div class="card-block">
            <table id="table-data" class="table table-bordered table-striped m-b-0" style="width:100%">
                <thead>
                <tr>
                    <th>PID</th>
                    <th>Process Name</th>
                    <th>Process Start</th>
                    <th>Process End</th>
                    <th>Status</th>
                    <th>Description</th>
                    <th>Menu</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function () {
            var table = $("#table-data").DataTable({

                serverSide: true,
                processing: true,
                order: [[1, "asc"]],
                buttons: [],
                ajax: {
                    url: "{{ url('home/allposts') }}",
                    // dataSrc: '',
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // data : function (data) {
                    //     return JSON.stringify(data) ;
                    // }
                },
                columns: [
                    {data: "pid", name: "pid", sortable: true, searchable: true},
                    {data: "process_name", name: "process_name", sortable: false},
                    {data: "process_start", name: "process_start", sortable: false},
                    {data: "process_end", name: "process_end", sortable: false},
                    {data: "status", name: "status", sortable: false},
                    {data: "description", name: "status", sortable: false},
                    {data: "pid", className: "dt-body-center", render: function(data, type, row, meta) {
                        return '<a href="<?php echo url('/')?>/home/terminate/'+data+'"><button class="btn btn-outline-primary m-r-xs m-b-xs btn-sm">Terminate</button></a>' ;
                    }, sortable: false, searchable: false}

                ]
            });
        });

        function tableData(data) {
            $("#table-data tbody").empty();
            $.each(data['content'], function (index, value) {
                var tr0 = "<tr>";
                var td0 = "<td>" + value.pid + "</td>";
                var td1 = "<td>" + value.process_name + "</td>";
                var td2 = "<td>" + value.process_start + "</td>";
                var td3 = "<td>" + value.process_end + "</td>";
                var td4 = "<td>" + value.status + "</td>";
                var td5 = "<td>" + value.description + "</td>";
                if(value.status == RUNNING){
                    var td6 = "<td>" +
                        "<button id='"+value.pid+"' class='btn btn-act btn-outline-primary btn-sm btn-detail-invoice'>Terminate</button>" +
                        "</td>";
                }else{
                    var td6 = "<td>"+"-"+"</td>";
                }

                var tr1 = "</tr>";

                var table = tr0 + td0 + td1 + td2 + td3 + td4 + td5+ td6 + tr1;
                $("#table-data tbody").append(table);

            });
        }


        $(document).delegate("#page-next", "click", function () {
            var no = $("#page-no > span ").text();
            var next = parseInt(no) + 1;
            loadData(next - 1, 20);
            $("#page-no > span ").text(next);
            return false;
        });

        $(document).delegate("#page-prev", "click", function () {
            var no = $("#page-no > span ").text();
            var prev = parseInt(no) - 1;
            loadData(prev - 1, 20);
            $("#page-no > span ").text(prev);
            return false;
        });

    </script>
@endsection