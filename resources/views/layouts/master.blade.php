<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content=""/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1"/>
    <meta name="msapplication-tap-highlight" content="no">

    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Milestone">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Milestone">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="theme-color" content="#4C7FF0">

    <title>Billing Support System</title>

    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/select2.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/gsi-step-indicator.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/tsf-step-form-wizard.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap4.css')}}" type='text/css' />

    <link rel="stylesheet" href="{{ asset('css/appui.css') }}"/>
</head>
<style>
    .brand .brand-logo img {
        max-height: 100px !important;
    }

    .configuration-cog {
        display: none !important;
    }
</style>
<body>

<div class="app">
    <!--sidebar panel-->
    <div class="off-canvas-overlay" data-toggle="sidebar"></div>
    <div class="sidebar-panel">
        <div class="brand">
            <!-- toggle offscreen menu -->
            <a href="javascript:;" data-toggle="sidebar"
               class="toggle-offscreen hidden-lg-up"> <i
                        class="material-icons">line_weight</i>
            </a>
            <!-- /toggle offscreen menu -->
            <!-- logo -->
            <a class="brand-logo"> <img class="expanding-hidden" src="{{ asset('images/logo-play.png') }}"
                                        alt="MNC Playmedia"/>
            </a>
            <!-- /logo -->
        </div>
        <div class="nav-profile dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                <div class="user-info expanding-hidden">
                    <small class="bold">{{session('user')->email}}</small>
                </div>
            </a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="javascript:;">Settings</a>
                {{--<a class="dropdown-item" href="javascript:;">Upgrade</a>--}}
                {{--<a class="dropdown-item" href="javascript:;">--}}
                {{--<span>Notifications</span>--}}
                {{--<span class="tag bg-primary">34</span>--}}
                {{--</a>--}}
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ url('postlogout') }}"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit()">Logout</a>
                <form id="logout-form" action="{{ url('postlogout') }}" method="GET" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
        <!-- main navigation -->
        <nav>
            @foreach(session('menu') as $menu)
                <ul class="nav">
                    <!-- Preparation -->
                    <li><a href="javascript:;"> <span class="menu-caret">
                            <i class="material-icons">arrow_drop_down</i>
						</span> <i class="material-icons text-primary">line_weight</i> <span>{{$menu->name}}</span>
                        </a>
                        @foreach(session('submenu') as $sub )
                            <ul class="sub-menu">
                                @if($menu->id == $sub->status)
                                    <li>
                                        <a href="{{ url($sub->route) }}">
                                            <i class="material-icons text-primary">{{substr($sub->name,0,1)}}</i>
                                            <span>{{$sub->name}}</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        @endforeach

                    </li>
                </ul>
            @endforeach
        </nav>
        <!-- /main navigation -->
    </div>
    <!-- /sidebar panel -->
    <!-- content panel -->
    <div class="main-panel">
        <!-- top header -->
        <nav class="header navbar">
            <div class="header-inner">
                <div class="navbar-item navbar-spacer-right brand hidden-lg-up">
                    <!-- toggle offscreen menu -->
                    <a href="javascript:;" data-toggle="sidebar"
                       class="toggle-offscreen"> <i class="material-icons">menu</i>
                    </a>
                    <!-- /toggle offscreen menu -->
                    <!-- logo -->
                    <a class="brand-logo hidden-xs-down"> MNC Playmedia
                    </a>
                    <!-- /logo -->
                </div>
                <a
                        class="navbar-item navbar-spacer-right navbar-heading hidden-md-down"
                        href="#"> <span>
                        <?php
                        if (isset($title)) {
                            echo $title;
                        } else {

                            echo "Billing Support System";
                        }
                        ?>
                    </span>
                </a>
                <div class="navbar-search navbar-item" style="display: none;">
                    <form class="search-form">
                        <i class="material-icons">search</i> <input class="form-control"
                                                                    type="text" placeholder="Search"/>
                    </form>
                </div>
            </div>
        </nav>
        <!-- /top header -->

        <!-- main area -->
        <div class="main-content">
            <div class="content-view">

                @yield('content') </div>

            <!-- bottom footer -->
            <div class="content-footer">
                <nav class="footer-right">
                    <ul class="nav">
                        <li><a href="javascript:;">Feedback</a></li>
                    </ul>
                </nav>
                <nav class="footer-left">
                    <ul class="nav">
                        <li><a href="javascript:;"> <span>Copyright</span>
                                &copy; 2017 MNC Play Media
                            </a></li>
                        <li class="hidden-md-down"><a href="javascript:;">Privacy</a>
                        </li>
                        <li class="hidden-md-down"><a href="javascript:;">Terms</a>
                        </li>
                        <li class="hidden-md-down"><a href="javascript:;">help</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <!-- /bottom footer -->
        </div>
        <!-- /main area -->
    </div>
    <!-- /content panel -->


</div>

<script type="text/javascript">
    window.paceOptions = {
        document: true,
        eventLag: true,
        restartOnPushState: true,
        restartOnRequestAfter: true,
        ajax: {
            trackMethods: ['POST', 'GET']
        }
    };
</script>
<script src="{{ asset('js/appui.js') }}"></script>

<script src="{{ asset('js/sweetalert.min.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/daterangepicker.js') }}"></script>
<script src="{{ asset('js/select2.js') }}"></script>
<script src="{{ asset('js/parsley.min.js') }}"></script>
<script src="{{ asset('js/tsf-wizard-plugin.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.js')}}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.js')}}"></script>
</div>
</body>
</html>
@yield('javascript')