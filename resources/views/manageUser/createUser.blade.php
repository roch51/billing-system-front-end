@extends('layouts.master')
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header no-bg b-a-0">
                </div>
                <div class="card-block">
                    <form role="new" method="POST" action="{{ URL('user/created') }}"
                          enctype="multipart/form-data"/>
                    {{ csrf_field() }}

                    <fieldset class="form-group">
                        <label for="nama">
                            Nama
                        </label>
                        <input class="form-control" name="data[name]">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="Heading Indonesia">
                            Email
                        </label>
                        <input class="form-control" name="data[email]" type="email">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="Heading Indonesia">
                            Password
                        </label>
                        <input class="form-control" name="data[password]" type="password">
                    </fieldset>
                    <button type="submit" class="btn btn-primary">
                        Submit
                    </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
