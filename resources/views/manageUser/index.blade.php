@extends('layouts.master')
@section('content')
    <div class="card">
        <div class="card-block">
            <div class="table-responsive">
                <table class="table table-bordered table-striped m-b-0">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Menu</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->role}}</td>
                            <td>
                                 <span class="add-on input-group-addon">
                                          <a href="#"
                                             onclick="document.getElementById('user_id').value ='{{ ($user->id) }}'"
                                             id="popup-account" data-toggle='modal'
                                             data-target='.bd-example-modal-lg'>
										<i class="material-icons" aria-hidden="true">
                                            edit </i>
                                          </a>
									  </span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Select Role</h4>
                </div>
                <div class="modal-body">
                    <form role="form" method="POST" action="{{ url('/users/user/update') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="account-fullname">Select</label>
                            <select class="form-control" id="exampleSelect1" name="role">
                                <option value="0">--Choose--</option>
                                @foreach($roles as $role)
                                <option value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" value="{{$user->id}}" name="user_id" id="user_id">
                        </div>
                        <button type="submit" id="btn-search-account"
                                class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                            <i class="material-icons">send</i>
                            <span>Submit</span>
                        </button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
