@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header no-bg b-a-0"><h3>Account</h3></div>

                <div class="card-block">
                    <form id="preparationform" data-toggle="validator" role="form">
                        <div class="form-group">
                            <label for="exampleInputEmail1">
                                Account Id
                            </label>
                            <input class="form-control" id="account-id"
                                   placeholder="Account ID" required="" type="current" value="{{$data !=null? $data['id']:"-"}}" readonly="true"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">
                                Account Name
                            </label>
                            <input class="form-control" id="account-name"
                                   placeholder="Account Name" required="" type="current" value="{{$data!=null? $data['account']:"-"}}" readonly="true"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputAddress">
                                Account Address
                            </label>
                            <input class="form-control" id="account-address"
                                   placeholder="Account Address" required="" type="current" value="{{$data!=null? $data['address']:"-"}}" readonly="true"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPhone">
                                Account Phone
                            </label>
                            <input class="form-control" id="account-phone"
                                   placeholder="Account Phone" required="" type="current" value="{{$data!=null? $data['phone']:"-"}}" readonly="true"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail">
                                Account Email
                            </label>
                            <input class="form-control" id="account-phone"
                                   placeholder="Account Email" required="" type="current" value="{{$data!=null? $data['email']:"-"}}" readonly="true"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <section id="invoice">
                <div class="card alert">
                    <div class="card-header no-bg b-a-0">
                        <h3>Order</h3>

                    </div>
                    <div class="card-block">
                        <table id="table-data" class="table table-bordered table-striped m-b-0">
                            <thead>
                            <tr>
                                <th>Item Type</th>
                                <th>Item Description</th>
                                <th>Item value</th>
                                <th>Discount Name</th>
                                <th>Discount Amount</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <section id="invoice">
                <div class="card alert">
                    <div class="card-header no-bg b-a-0">
                        <h3>Discount</h3>
                    </div>
                    <div class="card-block">
                        <table id="table-data2" class="table table-bordered table-striped m-b-0">
                            <thead>
                            <tr>
                                <th>Item Description</th>
                                <th>Calculation</th>
                                <th>Discount End Date </th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
            </section>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function(){
//            Start Initialize form
            loadData() ;
            loadData2() ;

        });

        function  loadData(page,size) {
            var id = "<?php echo $data["id"]?>" ;
            var url = "<?php echo url('/')?>/resource/findaccount/"+id ;
//            alert(url);
            $.ajax({
                url: url,
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                //data: '{"foo":"bar"}',
                success: function (data) {
//                    alert(data);
                    tableData(data) ;
                },
                error: function(){
                    swal('Refresh', 'Failed load data order!', 'warning');
                }
            });
        }

        function tableData(data){
            $("#table-data tbody").empty();
            $.each(data,function(index,value){
                var ifNullNameDiscount = value.nameDiscount===null?'-':value.nameDiscount ;
                var ifNullDiscount = value.discount===null?0:value.discount ;

                var tr0 = "<tr>";
                var td0 = "<td>"+value.itemType+"</td>";
                var td1 = "<td>"+value.itemDescription+"</td>";
                var td2 = "<td>"+value.amount+"</td>";
                var td3 = "<td>"+ifNullNameDiscount+"</td>";
                var td4 = "<td>"+ifNullDiscount+"</td>";
                var tr1 = "</tr>" ;

                var table = tr0 + td0 + td1 + td2 + td3 + td4 + tr1 ;
                $("#table-data tbody").append(table);

            });
        }

        function  loadData2(page,size) {
            var id = "<?php echo $data["id"]?>" ;
            var url = "<?php echo url('/')?>/resource/listdiscount/"+id ;
//            alert(url);
            $.ajax({
                url: url,
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                //data: '{"foo":"bar"}',
                success: function (data) {
//                    alert(data);
                    tableData2(data) ;
                },
                error: function(){
                    swal('Refresh', 'Failed load data discount!', 'warning');
                }
            });
        }

        function tableData2(data){
            $("#table-data2 tbody").empty();
            $.each(data,function(index,value){
                var tr0 = "<tr>";
                var td0 = "<td>"+value.itemDescription+"</td>";
                var td1 = "<td>"+value.amountCalc+"</td>";
                var td2 = "<td>"+value.effectiveTo+"</td>";
                var tr1 = "</tr>" ;

                var table = tr0 + td0 + td1 + td2 + tr1 ;
                $("#table-data2 tbody").append(table);

            });
        }


    </script>
@endsection