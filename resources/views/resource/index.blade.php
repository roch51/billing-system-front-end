@extends('layouts.master')

@section('content')
    <div class="card">
        <div class="card-header no-bg b-a-0">
            List of Account
        </div>
        <div class="card-block">
            <table id="table-data" class="table table-bordered table-striped m-b-0" style="width:100%">
                <thead>
                <tr>
                    <th>Account ID </th>
                    <th>Account Name</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function(){
            var table = $("#table-data").DataTable({

                serverSide: true,
                processing: true,
                order: [[1, "asc"]],
                buttons: [],
                ajax: {
                    url: "<?php echo url('/')?>/resource/accounts/rest",
                    // dataSrc: '',
                    type : "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // data : function (data) {
                    //     return JSON.stringify(data) ;
                    // }
                },
                columns: [
                    {data: "id", name: "id", sortable: true,searchable:false},
                    {data: "account", name: "account", sortable: true},
                    {data: "address", name: "address", sortable: false},
                    {data: "email", name: "email", sortable: false},
                    {data: "phone", name: "phone", sortable: false},
                    {data: "id", className: "dt-body-center", render: function(data, type, row, meta) {
                            return '<a href="<?php echo url('/')?>/resource/detail/'+data+'"><button class="btn btn-outline-primary m-r-xs m-b-xs btn-sm">detail</button></a>' ;
                        }, sortable: false, searchable: false}

                ]
            });
        });

        function tableData(data){
            $("#table-data tbody").empty();
            $.each(data['content'],function(index,value){
                var tr0 = "<tr>";
                var td0 = "<td>"+value.id+"</td>";
                var td1 = "<td>"+value.account+"</td>";
                var td2 = "<td>"+value.address+"</td>";
                if(value.email == null){
                    var td3 = "<td>"+"-"+"</td>";
                }else{
                    var td3 = "<td>"+value.email+"</td>";
                }

                if(value.phone == null){
                    var td4 = "<td>"+"-"+"</td>";
                }else{
                    var td4 = "<td>"+value.phone+"</td>";
                }
                var td5 = "<td>" +

                    "<button id='"+value.id+"' class='btn btn-act btn-outline-primary btn-sm btn-detail-invoice'>Detail</button>" +
                    "</td>";
                var tr1 = "</tr>" ;

                var table = tr0 + td0 + td1 + td2 + td3 +td4 +  td5 + tr1 ;
                $("#table-data tbody").append(table);

            });
        }


        $(document).delegate("#page-next","click",function(){
            var no = $("#page-no > span ").text() ;
            var next = parseInt(no) + 1 ;
            loadData(next - 1,20) ;
            $("#page-no > span ").text(next);
            return false ;
        });

        $(document).delegate("#page-prev","click",function(){
            var no = $("#page-no > span ").text() ;
            var prev = parseInt(no) - 1 ;
            loadData(prev - 1,20) ;
            $("#page-no > span ").text(prev);
            return false ;
        });

        $(document).delegate(".btn-detail-invoice","click",function(){
            var id = this.id ;
            var urls =  "<?php echo url('/')?>/resource/detail/"+id ;
            window.location.href = urls;
            return false ;
        });
    </script>
@endsection