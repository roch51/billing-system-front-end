@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header no-bg b-a-0"></div>
                <div class="card-block">

                    <table id="table-popup" class="table table-bordered table-striped m-b-0">
                        <thead>
                        <tr>
                            <th>Invoice Number</th>
                            <th>Account Id</th>
                            <th>Account Name</th>
                            <th>Period</th>
                            <th>Billing Cycle Id</th>
                            <th>Invoice From</th>
                            <th>Invoice To</th>
                            <th>Invoice Due Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($result->data))
                            @foreach($result->data as $invoice)
                            <tr>
                                <td>{{$invoice->invoiceNumber}}</td>
                                <td>{{$invoice->accountId}}</td>
                                <td>{{$invoice->contactName}}</td>
                                <td>{{$invoice->period}}</td>
                                <td>{{$invoice->billingCycleId}}</td>
                                <td>{{$invoice->invoiceFrom}}</td>
                                <td>{{$invoice->invoiceTo}}</td>
                                <td>{{$invoice->dueDate}}</td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">No data found</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    <br />
                    <div class="col-lg-12">
                        Message :
                        <?php
                            $alert = "" ;
                            $headermessage = "" ;
                            if ($result->status == "success") {
                                $alert = "alert-success" ;
                                $headermessage = "<b>Well done! You successfully read this important alert message.</b>" ;
                            }else {
                                $alert = "alert-danger" ;
                                $headermessage = "<b>Oh snap! Change a few things up and try submitting again.</b>" ;
                            }
                        ?>
                        <div class="alert {{$alert}}">
                            <?php echo $headermessage ;?>
                            <br />
                            {{$result->message}}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('javascript')

@endsection