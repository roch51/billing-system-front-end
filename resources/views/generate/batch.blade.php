@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header no-bg b-a-0"></div>
                <div class="card-block">
                    <form method="POST" action="<?php echo url('/')?>/generate/batch/save" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="input-prepend input-group m-b-1">
									  <span class="add-on input-group-addon">
										    <i class="material-icons">
										     date_range
										    </i>
									  </span>
                            <input id="periode" name="periode" class="form-control" value="" placeholder="Periode" type="text" required="true">

                        </div>
                        <div class="input-prepend input-group m-b-1">
                            <input type="file" name="excel" id="excel"/>
                        </div>
                        <button id="proses" type="submit"  class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                            <i class="material-icons">send</i>
                            <span>Process</span>
                        </button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {
            $('#periode').datepicker( {
                format: "yyyymm",
                viewMode: "months",
                minViewMode: "months"
            });

            $("#proses").click(function () {
                swal({
                    title: 'Are you sure?',
                    text: 'please make sure accounts has not been generated !!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, do it!',
                    cancelButtonText: 'No, cancel!',
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function(isConfirm) {
                    if (isConfirm) {
                        swal('Generated!', 'Your batch file has been generated!', 'success');
                        setTimeout(function() {
                            $('form').submit() ;
                        }, 2000);
                    } else {
                        swal('Cancelled', 'Your batch file is safe :)', 'error');
                    }
                });

                return false ;
            }) ;
        });


    </script>
@endsection