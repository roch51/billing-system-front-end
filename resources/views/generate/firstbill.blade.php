@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header no-bg b-a-0"></div>
                <div class="card-block">
                    <form method="POST" action="/bss/public/preparation/batch/import" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="input-prepend input-group m-b-1">
									  <span class="add-on input-group-addon">
                                           <i class="material-icons">
										     search
										    </i>
									  </span>
                            <input id="account-id" name="account-id" class="form-control" value="" placeholder="Account Id" type="text" required="true" >
                        </div>
                        <button type="submit" id="btn-proses"  class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                            <i class="material-icons">send</i>
                            <span>Process</span>
                        </button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {

            $("#account-id").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything

                if (e.which != 118 && e.which != 97 && e.which != 99){
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                }
            });

           $("#btn-proses").click(function () {
               var id = $("#account-id").val() ;

               if (id != "") {
                   var urls = "<?php echo url('/')?>/generate/firstbill/exec/"+id;

                       swal({
                           title: 'Generate First Bill',
                           text: 'Account: '+id,
                           type: 'info',
                           showCancelButton: true,
                           closeOnConfirm: false,
                           showLoaderOnConfirm: true
                       }, function() {
                           $.ajax({
                               url: urls,
                               //beforeSend: function(xhr) {
                               //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                               //},
                               type: 'GET',
                               dataType: 'json',
                               contentType: 'application/json',
                               processData: false,
                               //data: '{"foo":"bar"}',
                               success: function (data) {
                                   console.log(data) ;
                                   setTimeout(function() {
                                       swal(data.message,data.status,'success');

                                   }, 2000);
                               },
                               error: function(data){
                                   swal(data.message, data.status, 'error');
                               }
                           });

                   });
                   return false ;
               }else {
                   return true ;
               }
           })
        });
    </script>
@endsection