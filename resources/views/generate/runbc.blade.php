@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header no-bg b-a-0"></div>
                <div class="card-block"><form action="/" id="runbillform" data-toggle="validator" role="form">
                        <div class="input-prepend input-group m-b-1">
									  <span class="add-on input-group-addon">
										    <i class="material-icons">
										     date_range
										    </i>
									  </span>
                            <input id="periode" name="periode" class="form-control" value="" placeholder="Periode" type="text" required="true">

                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12">
                                <select id="billingcycle" data-placeholder="Pick Your Billing Cycle" class="select2 m-b-1" style="width: 100%;" required="true">
                                    @foreach($mbillingCycles as $bc)
                                        <option value="{{$bc->id}}">{{$bc->billingCycle}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button type="submit" id="btn-generate" class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                            <i class="material-icons">send</i>
                            <span>Process</span>
                        </button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function(){

            $('.select2').select2();

            $('#periode').datepicker( {
                format: "yyyymm",
                viewMode: "months",
                minViewMode: "months"
            });

            $("#periode").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything

                if (e.which != 118 && e.which != 97 && e.which != 99){
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                }
            });


            $("#btn-generate").click(function () {
                var period = $("#periode").val() ;
                var bc = $("#billingcycle").val() ;


                if (period == "" || bc == "") {
                    return true ;
                }else {
                    var urls = "<?php echo url('/')?>/generate/runbc/exec/"+period+"/"+bc;

                    swal({
                        title: 'Generate Run Bill',
                        text: 'Period: '+period+' & bc :'+bc,
                        type: 'info',
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true
                    }, function() {
                        $.ajax({
                            url: urls,
                            //beforeSend: function(xhr) {
                            //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                            //},
                            type: 'GET',
                            dataType: 'json',
                            contentType: 'application/json',
                            processData: false,
                            //data: '{"foo":"bar"}',
                            success: function (data) {
                                console.log(data) ;
                                setTimeout(function() {
                                    swal(data.message,data.status,'success');

                                }, 2000);
                            },
                            error: function(data){
                                swal(data.message, data.status, 'error');
                            }
                        });

                    });

                    return false ;
                }
            });

        });
    </script>
@endsection