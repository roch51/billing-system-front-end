@extends('layouts.master')
@section('content')
    <div class="card">
        <div class="card-header no-bg b-a-0">
            <a href="{{URL('role/create')}}"
               title="Show">
                <i class="material-icons text-primary">add</i>
            </a>
        </div>
        <div class="card-block">
            <div class="table-responsive">
                <table class="table table-bordered table-striped m-b-0">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Routing</th>
                        <th>Menu</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($rolesMenu as $roleMenu)
                        @if($roleMenu->menu->status != 0)
                            <tr>
                                <th>{{$roleMenu->menu->name}}</th>
                                <th>{{$roleMenu->menu->route}}</th>
                                <th></th>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
