@extends('layouts.master')
@section('content')
    <div class="card">
        <div class="card-header no-bg b-a-0">
            <h3>List Menu Parent</h3>
                <button class="btn btn-default btn-sm" data-toggle="modal"
                        data-target=".bd-example-modal1">
                    Add New Menu Parent
                </button>
        </div>
        <div class="card-block">
            <div id="accordion" role="tablist" aria-multiselectable="true">
                @foreach($menus as $menu)
                    <div class="card panel panel-default m-b-xs">
                        <div class="card-header panel-heading panels parent" role="tab" id="{{$menu->id}}">

                            <h6 class="panel-title m-a-0">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$menu->id}}"
                                   aria-expanded="true"
                                   aria-controls="collapse{{$menu->id}}">
                                    {{$menu->name}}
                                </a>
                            </h6>
                        </div>
                        <div id="collapse{{$menu->id}}" class="card-block panel-collapse collapse "
                             role="tabpane1{{$menu->id}}"
                             aria-labelledby="{{$menu->id}}">
                            <div class="card-header no-bg b-a-0">
                                <button class="btn btn-default btn-sm" data-toggle="modal"
                                        data-target=".bd-example-modal">
                                   Add New Menu
                                </button>
                            </div>
                            <table id="table-data" class="table table-bordered table-striped m-b-0">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Route</th>
                                    <th>Menu</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Menu</h4>
                </div>
                <div class="modal-body">
                    <form role="form" method="POST" action="{{ url('/menu/child/add') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="Name">Name</label>
                            <input type="text" class="form-control"
                                   name="nama" />
                        </div>
                        <div class="form-group">
                            <label for="Route">Route</label>
                            <input type="text" class="form-control"
                                  name="route"/>
                            <input id="parent-id" type="hidden" class="parrent_id"
                                  name="parrent" value=""/>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Menu Parrent</h4>
                </div>
                <div class="modal-body">
                    <form role="form" method="POST" action="{{ url('/menu/parrent/add') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="Name">Name</label>
                            <input type="text" class="form-control"
                                   name="nama" />
                        </div>
                        <div class="form-group">
                            <label for="Route">Route</label>
                            <input type="text" class="form-control"
                                  name="route"/>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')
    <script>
        $("document").ready(function () {
            $('.panels').click(function () {
                var id = $(this).attr('id');
                $("#parent-id").val(id) ;
                loadData(id);
            });
        });

        function loadData(id) {
            var url = "<?php echo url('/')?>/menu/child/" + id;
//            alert(url);
            $.ajax({
                url: url,
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                //data: '{"foo":"bar"}',
                success: function (data) {
                    tableData(data);
                },
                error: function () {
                    swal('Refresh', 'Failed load data!', 'warning');
                }
            });
        }

        function tableData(data) {
            $("#table-data tbody").empty();
            $.each(data['0'], function (index, value) {
                var tr0 = "<tr>";
                var td0 = "<td>" + value.name + "</td>";
                var td1 = "<td>" + value.route + "</td>";
                var td2 = "<td>" +

                    "<button id='" + value.id + "' class='btn btn-act btn-outline-danger btn-sm btn-detail-invoice'>Delete</button>" +
                    "</td>";
                var tr1 = "</tr>";

                var table = tr0 + td0 + td1 + td2 + tr1;
                $("#table-data tbody").append(table);

            });
        }


        $(document).delegate(".btn-detail-invoice", "click", function () {
            var generateNo = this.id;
            var urls = "<?php echo url('/')?>/menu/child/delete/" + generateNo;
            window.location.replace(urls);
            return false;
        });

    </script>
@endsection