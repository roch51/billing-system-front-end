@extends('layouts.master')
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header no-bg b-a-0">
                </div>
                <div class="card-block">
                    <form role="new" method="POST" action="{{ URL('role/created') }}"
                          enctype="multipart/form-data"/>
                    {{ csrf_field() }}

                    <fieldset class="form-group">
                        <label for="nama">
                            Nama
                        </label>
                        <input class="form-control" name="data[name]">
                    </fieldset>
                    <button type="submit" class="btn btn-primary">
                        Submit
                    </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
