@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section id="blank">
                <div class="card alert">
                    <div class="card-header no-bg b-a-0">
                        <div class="alert alert-info" style="display:none">
                            ${message}
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="inputFrom">
                                    Date Range
                                </label>
                                <div class="input-prepend input-group m-b-1">
                                                <span class="add-on input-group-addon">
                                                    <i class="material-icons">
                                                      date_range
                                                    </i>
                                                </span>
                                    <input type="text" name="date-range-fb" id="date-range-fb" class="form-control drp" value="" placeholder="Date range picker"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <br />
                                This process is required to collect first bill information in BSS1, maybe take a long time due internet connection <br />
                                If you want to sync more than 1 month please contact your DATABASE ADMINISTRATOR / IT ADMINISTRATOR
                                <br /><br />
                                <button type="button" id="btn-collect-fb" class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                                    <i class="material-icons">send</i>
                                    <span>Collect data</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function(){
            $('#date-range-fb').daterangepicker({
                timePicker: false,
                timePickerIncrement: 30,
                locale: {
                    format: 'YYYY/MM/DD'
//                   format: 'MM/DD/YYYY h:mm A'
                }
            });

        });


        $(document).delegate("#btn-collect-fb","click",function () {
            var daterange = $("#date-range-fb").val().split("-") ;
            var from = (daterange[0].replace(/\s+/g, '')).replace(new RegExp('/', 'g'),'-') ;
            var to = (daterange[1].replace(/\s+/g, '')).replace(new RegExp('/', 'g'),'-') ;
            var urls = "<?php echo url('/')?>/payment/firstbill/"+from+"/"+to ;

            swal({
                title: 'Sync First Bill BSS1',
                text: 'Previous payment : '+$("#date-range-fb").val(),
                type: 'info',
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function() {
                $.ajax({
                    url: urls,
                    //beforeSend: function(xhr) {
                    //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                    //},
                    type: 'GET',
                    dataType: 'json',
                    contentType: 'application/json',
                    processData: false,
                    //data: '{"foo":"bar"}',
                    success: function (data) {
                        setTimeout(function() {
                            swal(data.message,data.status,'success');
                        }, 2000);
                    },
                    error: function(xhr,data){
                        swal("Whoops, looks like something went wrong", "Status : "+xhr.status, 'error');
                    }
                });
            });
        });
    </script>
@endsection