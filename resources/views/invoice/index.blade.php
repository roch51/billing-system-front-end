@extends('layouts.master')

@section('content')
    <div class="card">
        <div class="card-header no-bg b-a-0">
            List of Invoice
        </div>
        <div class="card-block">
            <table id="table-data" class="table table-bordered table-striped m-b-0" style="width:100%">
                <thead>
                <tr>
                    <th>Periode</th>
                    <th>Generate No</th>
                    <th>Billing Generate Type ID</th>
                    <th>Billing Cycle ID</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myProgresBarModal" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myProgressBarTitle">Generate PDF Prpgress Bar</h4>
                </div>
                <div class="modal-body">
                    <progress id='pdf-progress' class='progress progress-striped' value='0' max='100'></progress>
                    <input type="hidden" id="generate-no" />
                    <button type="button" id="btn-pdf-generate" class="btn btn-primary btn-icon loading-demo m-r-xs m-b-xs btn-sm">
                        <i class="material-icons">send</i>
                        <span>Run</span>
                    </button>
                    <br />
                    <small class="p-t-2">

                        <div class="alert alert-warning">
                            Warning ! when you run, don't close browser, until finish !!
                        </div>
                    </small>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function(){

            var table = $("#table-data").DataTable({
                serverSide: true,
                processing: true,
                order: [[1, "desc"]],
                buttons: [],
                ajax: {
                    url: "<?php echo url('/')?>/invoice/loadgenerate",
                    type : "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                },
                columns: [
                    {data: "periode", name: "period", sortable: true,searchable:true},
                    {data: "generateNo", name: "generateNo", sortable: true},
                    {data: "generateTypeId", name: "generateTypeId", sortable: false},
                    {data: "billingCycleId", name: "billingCycleId", sortable: false},
                    {data: "generateNo", className: "dt-body-center", render: function(data, type, row, meta) {
                            return '' +
                                '<a href="<?php echo url("/")?>/invoice/summary/'+data+'"><button class="btn btn-info btn-icon btn-sm btn-detail-invoice"><i class="material-icons">search</i>Summary</button></a>&nbsp;' +
                                '<button id='+data+' class=\'btn btn-danger btn-icon btn-sm btn-pdf\' data-toggle=\'modal\' data-target=\'.bd-example-modal-lg\'><i class=\'material-icons\'>import_export</i>PDF</button>' ;
                        }, sortable: false, searchable: false}

                ]
            });
        });

        $(document).delegate(".btn-pdf","click",function(){
            var generateNo = this.id ;
            $("#generate-no").val(generateNo) ;
            return false ;
        });

        $(document).delegate("#btn-pdf-generate","click",function () {
            var progress = $("#pdf-progress").val() ;
            var generateNo = $("#generate-no").val() ;
            var urls = "<?php echo url('/')?>/invoice/pdf/select/"+generateNo ;
            var datas = new Object() ;
            $("#"+generateNo).html("<i class='material-icons'>import_export</i>running...") ;
            $("#btn-pdf-generate").attr("disabled","disabled") ;
            $.ajax({
                url: urls,
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                //data: '{"foo":"bar"}',
                success: function (data) {
                    var max = data['size'] ;

                    $.each(data['content'],function(index,value){
                        $.ajax({
                            url: "<?php echo url('/')?>/invoice/pdf/single/"+value.id ,
                            type: 'GET',
                            dataType: 'json',
                            contentType: 'application/json',
                            processData: false,
                            //data: '{"foo":"bar"}',
                            success: function (data) {
                                if (data.status != 0) {
                                    var progress = index + 1 ;
                                    var persen = (progress / max ) * 100 ;
                                    $("#pdf-progress").val(persen) ;
                                }

                                if (persen == 100) {
                                    swal("done", "200", 'success');
                                    $("#"+generateNo).html("<i class='material-icons'>import_export</i>PDF") ;
                                    $("#btn-pdf-generate").removeAttr('disabled') ;
                                }
                            },
                            error: function(data){
                                swal(data.message, data.status, 'error');
                            }
                        });

                        setTimeout(function() {
                            console.log("test");
                        }, 4000);
                    }) ;

                },
                error: function(data){
                    swal(data.message, data.status, 'error');
                }
            }).done(dones(generateNo));

            return false ;
        });

        function dones(generateNo) {
            $("#"+generateNo).html("<i class='material-icons'>import_export</i>PDF") ;
            $("#btn-pdf-generate").removeAttr('disabled') ;
        }
    </script>
@endsection