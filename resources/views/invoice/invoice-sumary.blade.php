@extends('layouts.master')

@section('content')

        <div class="row">
        <div class="col-lg-12">
            <section id="invoice">
                <div class="card alert">
                    <div class="card-header no-bg b-a-0">
                        <nav>
                            <li class="page-item" id="page-prev">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">«</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item" ><a class="page-link" href="#" id="page-no"><span>1</span></a></li>

                            <li class="page-item" id="page-next">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">»</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </nav>
                    </div>
                    <div class="card-block">
                        <table id="table-data" class="table table-bordered table-striped m-b-0">
                            <thead>
                            <tr>
                                <th>Account Id</th>
                                <th>Contact Name</th>
                                <th>Due Date</th>
                                <th>Total Amount</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
            </section>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function(){
//            Start Initialize form
            var page = 0 ;
            var size = 20 ;
            $('.select2').select2();

            $('#periode').datepicker( {
                format: "mmyyyy",
                viewMode: "months",
                minViewMode: "months"
            });

            $('#invoice-period').datepicker( {
                format: "mmyyyy",
                viewMode: "months",
                minViewMode: "months"
            });

            $('#invoice-duedate').datepicker( {
                format: "yyyy-mm-dd"
            });

//            end of Initialize form
            loadData(page,size) ;

        });

        function  loadData(page,size) {
            var generateNo = "<?php echo $generateNo ?>" ;
            var url = "<?php echo url('/')?>/invoice/sumary/load/"+generateNo+"/"+page+"/"+size ;
            $.ajax({
                url: url,
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                //data: '{"foo":"bar"}',
                success: function (data) {
                    tableData(data) ;
                },
                error: function(){
                    swal('Refresh', 'Failed load data!', 'warning');
                }
            });
        }

        function tableData(data){
            $("#table-data tbody").empty();
            $.each(data['content'],function(index,value){
                var pdfdisabled = "" ;
                var emaildisabled = "" ;
                if (value.isPdf) {
                    pdfdisabled = "disabled" ;
                }

                if (value.isEmail) {
                    emaildisabled = "disabled" ;
                }
                var tr0 = "<tr>";
                var td0 = "<td>"+value.accountId+"</td>";
                var td1 = "<td>"+value.contactName+"</td>";
                var td2 = "<td>"+value.dueDate+"</td>";
                var td3 = "<td>"+value.amount+"</td>";
                var td5 = "<td>" +
                    "<button id='"+value.id+"' class='btn btn-info btn-icon btn-sm btn-detail-invoice'><i class='material-icons'>search</i><a href='<?php echo url('/')?>/invoice/sumary/detail/"+value.id+"'>Detail</a></button> &nbsp;" +
                    "<button "+pdfdisabled+" id='"+value.id+"' class='btn btn-danger btn-icon btn-sm btn-detail-pdf'><i class='material-icons'>import_export</i>PDF</button> &nbsp;" +
                    "<button "+emaildisabled+" id='"+value.id+"' class='btn btn-danger btn-icon btn-sm btn-detail-email'><i class='material-icons'>email</i>e-Mail</button> &nbsp;" +
                    "</td>";
                var tr1 = "</tr>" ;

                var table = tr0 + td0 + td1 + td2 + td3 +  td5 + tr1 ;
                $("#table-data tbody").append(table);

            });
        }

        $(document).delegate("#page-next","click",function(){
            var no = $("#page-no > span ").text() ;
            var next = parseInt(no) + 1 ;
            loadData(next - 1,20) ;
            $("#page-no > span ").text(next);
            return false ;
        });

        $(document).delegate("#page-prev","click",function(){
            var no = $("#page-no > span ").text() ;
            var prev = parseInt(no) - 1 ;
            loadData(prev - 1,20) ;
            $("#page-no > span ").text(prev);
            return false ;
        });

        $(document).delegate(".btn-detail-pdf","click",function(){
            var invId = this.id ;
            var urls = "<?php echo url('/')?>/invoice/pdf/single/"+invId ;
            $.ajax({
                url: urls,
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                //data: '{"foo":"bar"}',
                success: function (data) {
                    setTimeout(function() {
                        $("#pdf-progress").val(50) ;
                    }, 2000);
                    setTimeout(function() {
                        swal(data.message,data.status,'success');
                        $("#pdf-progress").val(100) ;
                    }, 2000);
                },
                error: function(data){
                    swal(data.message, data.status, 'error');
                }
            }) ;
            return false ;
        });

        $(document).delegate(".btn-detail-email","click",function(){
            var invId = this.id ;
            alert(invId)
            return false ;
        });
    </script>
@endsection