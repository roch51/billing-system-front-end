@extends('layouts.master')

@section('content')

    <div class="card col-lg-6">
        <div class="card-block">
            <?php
                $pdfName = $data['InvoiceSummary']['period'].$data['InvoiceSummary']['accountId'].".pdf" ;
            ?>
            <embed src="{{getenv('SERVERDEV')}}/{{$data['InvoiceSummary']['generateNo']}}/{{$pdfName}}"
                   width="700" height="700" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">
        </div>
    </div>
@endsection

@section('javascript')
    <script>

    </script>
@endsection