@extends('layouts.master')

@section('content')
    <div class="card">
        <div class="card-header no-bg b-a-0">
            List of Invoice Summary
        </div>
        <div class="card-block">
            <table id="table-data" class="table table-bordered table-striped m-b-0" style="width:100%">
                <thead>
                <tr>
                    <th>Generate No</th>
                    <th>Account Id</th>
                    <th>Contact Name</th>
                    <th>Due Date</th>
                    <th>Total Amount</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function () {
            var generateNo = "<?php echo $generateNo ;?>" ;
            var table = $("#table-data").DataTable({
                serverSide: true,
                processing: true,
                order: [[1, "desc"]],
                buttons: [],
                ajax: {
                    url: "<?php echo url('/')?>/invoice/summary/rest/"+generateNo,
                    type : "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                },
                columns: [
                    {data: "generateNo", name: "generateNo", sortable: true,searchable:true,visible:false},
                    {data: "accountId", name: "accountId", sortable: true,searchable:true},
                    {data: "contactName", name: "contactName", sortable: true},
                    {data: "dueDate", name: "dueDate", sortable: false},
                    {data: "totalAmount", name: "totalAmount", sortable: false},
                    {data: "id", className: "dt-body-center", render: function(data, type, row, meta) {
                            return '' +
                                '<a href="<?php echo url("/")?>/invoice/summary/detail/'+data+'"><button class="btn btn-info btn-icon btn-sm btn-detail-invoice"><i class="material-icons">search</i>Detail</button></a>&nbsp;'
                                // '<button id=\'"+data+"\' class=\'btn btn-danger btn-icon btn-sm btn-pdf\' data-toggle=\'modal\' data-target=\'.bd-example-modal-lg\'><i class=\'material-icons\'>import_export</i>PDF</button>' ;
                        }, sortable: false, searchable: false}

                ]
            });
        });

        function tableData(data) {
            $("#table-data tbody").empty();
            $.each(data['content'], function (index, value) {
                var pdfdisabled = "";
                var emaildisabled = "";
                if (value.isPdf) {
                    pdfdisabled = "disabled";
                }

                if (value.isEmail) {
                    emaildisabled = "disabled";
                }
                var tr0 = "<tr>";
                var td0 = "<td>" + value.accountId + "</td>";
                var td1 = "<td>" + value.contactName + "</td>";
                var td2 = "<td>" + value.dueDate + "</td>";
                var td3 = "<td>" + value.amount + "</td>";
                var td5 = "<td>" +
                    "<button id='" + value.id + "' class='btn btn-info btn-icon btn-sm btn-detail-invoice'><i class='material-icons'>search</i><a href='<?php echo url('/')?>/invoice/sumary/detail/" + value.id + "'>Detail</a></button> &nbsp;" +
                    "<button " + pdfdisabled + " id='" + value.id + "' class='btn btn-danger btn-icon btn-sm btn-detail-pdf'><i class='material-icons'>import_export</i>PDF</button> &nbsp;" +
                    "<button " + emaildisabled + " id='" + value.id + "' class='btn btn-danger btn-icon btn-sm btn-detail-email'><i class='material-icons'>email</i>e-Mail</button> &nbsp;" +
                    "</td>";
                var tr1 = "</tr>";

                var table = tr0 + td0 + td1 + td2 + td3 + td5 + tr1;
                $("#table-data tbody").append(table);

            });
        }

        $(document).delegate("#page-next", "click", function () {
            var no = $("#page-no > span ").text();
            var next = parseInt(no) + 1;
            loadData(next - 1, 20);
            $("#page-no > span ").text(next);
            return false;
        });

        $(document).delegate("#page-prev", "click", function () {
            var no = $("#page-no > span ").text();
            var prev = parseInt(no) - 1;
            loadData(prev - 1, 20);
            $("#page-no > span ").text(prev);
            return false;
        });

        $(document).delegate(".btn-detail-pdf", "click", function () {
            var invId = this.id;
            var urls = "<?php echo url('/')?>/invoice/pdf/single/" + invId;
            $.ajax({
                url: urls,
                //beforeSend: function(xhr) {
                //xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password"));
                //},
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                //data: '{"foo":"bar"}',
                success: function (data) {
                    setTimeout(function () {
                        $("#pdf-progress").val(50);
                    }, 2000);
                    setTimeout(function () {
                        swal(data.message, data.status, 'success');
                        $("#pdf-progress").val(100);
                    }, 2000);
                },
                error: function (data) {
                    swal(data.message, data.status, 'error');
                }
            });
            return false;
        });

        $(document).delegate(".btn-detail-email", "click", function () {
            var invId = this.id;
            alert(invId)
            return false;
        });
    </script>
@endsection