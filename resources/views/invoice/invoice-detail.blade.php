@extends('layouts.master')

@section('content')

    <div class="card">
        <div class="card-block">
            <div class="p-b-2 clearfix">
                <div class="pull-right text-xs-right">
                    <h5 class="bold m-b-0">
                        Invoice : {{$data["InvoiceSummary"]["invoiceNumber"]}}
                    </h5>
                    <p class="m-b-0">
                        Frekuensi Pembayaran : Bulanan
                    </p>
                    <p class="m-b-0">
                        Tanggal Cetak : {{$data["InvoiceSummary"]["invoiceDate"]}}
                    </p>
                    <p class="m-b-0">
                        Tanggal Jatoh Tempo {{$data["InvoiceSummary"]["dueDate"]}}
                    </p>
                    <p class="m-b-0">
                        Invoice period {{$data["InvoiceSummary"]["period"]}}
                    </p>
                </div>
                <div class="circle-icon bg-success text-white m-r-1">
                    <i class="material-icons">
                        archive
                    </i>
                </div>
                <div class="overflow-hidden">
                    <p class="m-b-0">
                        PT. MNC Kabel Mediacom
                    </p>
                    <p class="m-b-0">
                        NPWP : 03.256.238.1-021.000
                    </p>
                    <p class="m-b-0">
                        MNC Tower lantai 10,11 dan 12A
                    </p>
                    <p class="m-b-0">
                        Jl. Kebon sirih no.17-19 <br/> Jakarta Pusat, 10340
                    </p>
                </div>
            </div>
            <div class="p-t-2 p-b-2 clearfix">
                <img src="{{asset('images/avatar.jpg')}}" class="avatar avatar-xs img-circle m-r-1 pull-left" alt="user"
                     title="user"/>
                <div class="overflow-hidden">
                    <p class="m-b-0">
                        <strong>
                            Client Details
                        </strong>
                    </p>
                    <p class="m-b-0">
                        {{$data["InvoiceSummary"]["accountId"]}}
                    </p>
                    <p class="m-b-0">
                        {{$data["InvoiceSummary"]["contactName"]}}
                    </p>
                    <p class="m-b-0">
                        {{$data["InvoiceSummary"]["address"]}}
                    </p>
                    <p class="m-b-0">
                        {{$data["InvoiceSummary"]["kota"].",".$data["InvoiceSummary"]["zipCode"]}}
                    </p>
                </div>
            </div>
            <div class="table-responsive p-t-2 p-b-2">
                <table class="table table-bordered m-b-0">
                    <thead>
                    <tr>
                        <th>
                            Tanggal
                        </th>
                        <th>
                            Type
                        </th>
                        <th>
                            Rincian Transaksi
                        </th>
                        <th>
                            Jumlah
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $data['InvoiceComponents'] as $component)
                        <tr>
                            <td>{{$component['created']}}</td>
                            <td>{{$component['itemType']}}</td>
                            <td>{{$component['itemDescription']}}</td>
                            <td>{{$component['amount']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="invoice-totals p-t-2 p-b-2">
                <div class="invoice-totals-row">
                    <strong class="invoice-totals-title">
                        Previous Amount
                    </strong>
                    <span class="invoice-totals-value">
                        {{$data["InvoiceSummary"]["previousAmount"]}}
                    </span>
                </div>
                <div class="invoice-totals-row">
                    <strong class="invoice-totals-title">
                        Amount
                    </strong>
                    <span class="invoice-totals-value">
                        {{$data["InvoiceSummary"]["amount"]}}
                    </span>
                </div>
                <div class="invoice-totals-row">
                    <strong class="invoice-totals-title">
                        Diskon
                    </strong>
                    <span class="invoice-totals-value">
                        {{$data["InvoiceSummary"]["discount"]}}
                    </span>
                </div>
                <div class="invoice-totals-row">
                    <strong class="invoice-totals-title">
                        PPN
                    </strong>
                    <span class="invoice-totals-value">
                        {{$data["InvoiceSummary"]["ppn"]}}
                    </span>
                </div>
                <div class="invoice-totals-row">
                    <strong class="invoice-totals-title">
                        Total Amount
                    </strong>
                    <span class="invoice-totals-value">
                        {{$data["InvoiceSummary"]["totalAmount"]}}
                    </span>
                </div>
                <div class="invoice-totals-row">
                    <strong class="invoice-totals-title">
                        {{$data["InvoiceSummary"]["beCalculated"]}}
                    </strong>
                </div>
            </div>
            <small class="p-t-2">
                <strong>
                    PAYMENT TERMS AND POLICIES
                </strong>
                All accounts are to be paid within 7 days from receipt of invoice. To be paid by cheque or credit card
                or direct payment online. If account is not paid within 7 days the credits details supplied as
                confirmation of work undertaken will be charged the agreed
                quoted fee noted above. If the Invoice remails unpaid. our dept recovery agency, Urban, may charge you a
                fee of 25% of the unpaid portion of the
                invoice amount and other legal and collection costs not covered by the fee.
            </small>
        </div>
        <div class="card-footer text-xs-right">
            <button type="button" class="btn btn-danger btn-icon btn-sm" onclick="window.print();">
                <i class="material-icons">print</i>
                Print
            </button>
            <button type="button" class="btn btn-info btn-icon btn-sm">
                <i class="material-icons">import_export</i>
                <a href="/invoice/summary/pdf/{{$data["InvoiceSummary"]["id"]}}">PDF view</a>
            </button>
        </div>
    </div>
@endsection

@section('javascript')
    <script>

    </script>
@endsection