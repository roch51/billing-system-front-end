<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1"/>
    <meta name="msapplication-tap-highlight" content="no">

    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Milestone">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Milestone">

    <meta name="theme-color" content="#4C7FF0">

    <title>Billing System</title>

    <!-- page stylesheets -->
    <!-- end page stylesheets -->

    <!-- build:css({.tmp,app}) styles/app.min.css -->
    <link rel="stylesheet" href="{{ asset('css/appui.css') }}" />
    <!-- endbuild -->
</head>
<body>

<div class="app no-padding no-footer layout-static">
    <div class="session-panel">
        <div class="session">
            <div class="session-content">
                <div class="card card-block form-layout">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/postlogin') }}">
                        {{ csrf_field() }}
                        <div class="text-xs-center m-b-3">
                            <img src="{{ URL('images') }}/logoMkmBig.png" height="80" alt="" class="m-b-1"/>
                        </div>
                        <fieldset class="form-group">
                            <label for="email">
                                Enter your email
                            </label>
                            <input id="email" type="email" class="form-control form-control-lg" name="email" value="{{ old('email') }}" required autofocus>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="password">
                                Enter your password
                            </label>
                            <input id="password" type="password" class="form-control form-control-lg" name="password" placeholder="********"required>
                        </fieldset>
                        <button class="btn btn-primary btn-block btn-lg" type="submit">
                            Login
                        </button>
                    </form>
                </div>
            </div>
            <footer class="text-xs-center p-y-1">
                <p>
                    <a href="">
                        Forgot password?
                    </a>
                    &nbsp;&nbsp;·&nbsp;&nbsp;
                    <a href="">
                        Create an account
                    </a>
                </p>
            </footer>
        </div>

    </div>
</div>

<script type="text/javascript">
    window.paceOptions = {
        document: true,
        eventLag: true,
        restartOnPushState: true,
        restartOnRequestAfter: true,
        ajax: {
            trackMethods: [ 'POST','GET']
        }
    };
</script>

<!-- page scripts -->
<script src="{{ asset('js/appui.js') }}"></script>
{{--<script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>--}}
<!-- end page scripts -->

<!-- initialize page scripts -->
<script type="text/javascript">
//    $('#validate').validate();
</script>
<!-- end initialize page scripts -->

</body>
</html>
