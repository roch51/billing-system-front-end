@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section id="blank">
                <div class="card alert">
                    <div class="card-header no-bg b-a-0">
                        <div class="alert alert-info" style="display:none">
                            ${message}
                        </div>
                    </div>
                    <div class="card-block">
                        Welcome to bs checking after generate page,
                        <br />
                        <br />
                        Menu Invoice for generate bs_invoice to bs_checking_after_generate
                        <br />
                        Menu Report for generate bs_checking_after_genarete results to excel sheet
                        <br />

                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("document").ready(function(){
//           alert('test') ;
        });
    </script>
@endsection